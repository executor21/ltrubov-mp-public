These is the source for Lev Trubov's Project created in partial requirements
for the degree of Master of Science in Computer Science at San Franscisco 
State University.

The academic advisor for this project was Dr. Jozo Dujmovic, Professor of 
Computer Science.

This project contains the following:

The code/ directory, containing all source code required to generate the data
used in the project. The environment required for running this code is 
described in Appendix E of the project report.

The report/ directory contains the report.pdf file, a formatted version of the
project report suitable for reading or print. It also contains the source/
directory, containing an editable report.tex file, from which the report.pdf
file was generated by LaTex, and an images/ directory storing images placed in
the report.

IMPORTANT: The report includes, in Appendices D and F, the contents of JSON
files containing raw data results, and the contents of source code files in
various programming languages. This data is not contained in the .tex file,
but pulled directly from the appropriate location in the code/ directory.
Therefore, it relies on the files being present in the appropriate relative
location; for a succeful build, the ../../code/ directory relative to the .tex
file must exist and contain all the relevant subdirectories and files.

To generate a complete report, with all the references and table of contents,
LaTex typesetting should be run twice; the first run will generate auxiliary
files but will not place the coresponding data in the generated pdf.

NOTE: The source directory for GNU gzip, code/macro/gzip/, referenced in
Section 6.2.1 and Appendix D.3, has had the directories containing files
specific to MS-DOS, PRIMOS, and VMS operating systems removed. This was done to
ease distribution of the project, as these directories contain files commonly
blocked by email servers when send via email attachments. This project
conducted no experiments involving those operating systems, and the removal of
these directories has not affected the ability to build the software to
generate the data presented. If necessary, one can download the missing files
from ftp.gnu.org/gnu/gzip/, version 1.2.4. The only changes made within this
project to the gzip codebase were done in the Linux version of the Makefile.

The presentation/ directory contains the PowerPoint file used by Lev Trubov in his oral presentation in defence of the project.