#include "llvm/IR/Instructions.h"
#include <iostream>

using namespace llvm;
using namespace std;

int main(){
  for(int i = 0; i <= 64; i++){
    //Instruction::getOpcodeName(i);
    const char* val = Instruction::getOpcodeName(i);
    if(val[0] == '<' && i != 0){
      continue;
    }
    std::cout << i << ": " << val << '\n';
  }

  return 0;
}
