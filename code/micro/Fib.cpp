#include "utils.h"

int Fib::fib(int n){
	if(n <= 1){
		return n;
	}else{
		return fib(n-1) + fib(n-2);
	}
}

Fib::Fib(int t){
	target = t;
	result = -1;
}

void Fib::workload(){
	result = fib(target);
}

Fib::~Fib(){}