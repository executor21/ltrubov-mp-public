#include "utils.h"

BSearch::BSearch(const char* numberFile){
	arr = loadDoubleFileAndTarget(numberFile, &size, &target);
	result = -1;
}

void BSearch::workload(){
	long result = -1L;
	long lo = 0L;
	long hi = size-1;

	long mid;
	double mv;

	while(lo < hi){
		mid = (lo + hi)/2;
		mv = arr[mid];
		if(mv == target){ //can't use exact equality after file parsing
			result = mid;
			break;
		}else if(mv > target){
			hi = mid-1;
		}else{
			lo = mid+1;
		}
	}
}

BSearch::~BSearch(){
	delete[] arr;
}
