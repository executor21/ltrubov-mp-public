#include<stdlib.h>
#include<stdio.h>

#define N 10001L

long *squares;

void workload(){
	long t;
	for(long i = 0L; i < N; i++){
		t = i - 5000L;
		squares[i] = t*t;
	}
}

int main(){
	squares = new long[N];
	workload();
}
