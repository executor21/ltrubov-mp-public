#include "utils.h"

MMul::MMul(long N, int lv, int hv){
	size = N;

	matA = new int[N*N];
	matB = new int[N*N];
	matC = new int[N*N];
	
	int diff = hv - lv;
	int valA, valB;
	
	for(long i = 0L; i < N; i++){
		valA = lv + i%diff;
		valB = hv - i%diff;
		
		for(long j = 0L; j < N; j++){
			matA[i*N+j] = valA++;
			matB[i*N+j] = valB--;
			
			valA = (valA > hv ? lv : valA);
			valB = (valB < lv ? hv : valB);
			
			matC[i*N+j] = 0;
		}
	}
}

void MMul::workload(){
	for(long ii = 0L; ii < size*size; ii++){
		matC[ii] = 0;
	}

	for(long i = 0L; i < size; i++){
		for(long j = 0L; j < size; j++){
			for(long k = 0L; k < size; k++){
				matC[i*size+j] += matA[i*size+k]*matB[k*size+j];
			}
		}
	}
}

MMul::~MMul(){
	delete[] matA;
	delete[] matB;
	delete[] matC;
}
