#include "utils.h"

QS::QS(const char* numberFile){
	arr = loadIntegerFile(numberFile, &size);
	workCopy = (int*)malloc(size*sizeof(int));
}

void QS::swapArrayValues(long i, long j){
	if(i != j){
		int temp = workCopy[i];
		workCopy[i] = workCopy[j];
		workCopy[j] = temp;
	}
}

long QS::partitionArray(long lo, long hi){
	int pivot = workCopy[hi];
	long i = lo;
	
	for(long j = lo; j < hi; j++){
		if(workCopy[j] <= pivot){
			swapArrayValues(i,j);
			i++;
		}
	}
	
	swapArrayValues(i,hi);
	return i;
}

void QS::quicksortArray(long lo, long hi){
	if(lo < hi){
		long p = partitionArray(lo, hi);
		quicksortArray(lo, p-1);
		quicksortArray(p+1, hi);
	}
}

/*
int QS::valueAt(int index){
	return workCopy[index];
}
//*/

void QS::workload(){
	for(long i = 0; i < size; i++){
		workCopy[i] = arr[i];
	}
	quicksortArray(0, size-1);
}

QS::~QS(){
	delete[] arr;
	delete[] workCopy;
}
