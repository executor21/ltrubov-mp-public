#include "utils.h"

Text::Text(const char* fileName, const char* searchTag){
	words = loadLinesOfFile(fileName, &size);
	tag = searchTag;

	found = NULL;
	count = 0;
}

void Text::workload(){
	int currLen;
	int tagLen = customStrLen(tag);
	int j,k;

	Wordlist *last = NULL;

	for(int i = 0; i < size; i++){
		currLen = customStrLen(words[i]);
		if(currLen < tagLen){
			continue; //word can't end with tag because it's shorter
		}

		
		for(j = currLen-1, k = tagLen-1; j > currLen-1-tagLen; j--,k--){
			if(words[i][j] != tag[k]){
				break;
			}
		}

		//word does not end with tag
		if(k >= 0){
			continue;
		}

		count++;
		Wordlist *wd  = (Wordlist *)malloc(sizeof(Wordlist));
		wd->word = words[i];
		wd->next = NULL;

		if(last){
			last->next = wd;
			last = wd;
		}else{
			found = wd;
			last = found;
		}
	}
}

Text::~Text(){
	for(int i = 0; i < size; i++){
		delete[] words[i];
	}
	delete[] words;
	
	while(found){
		Wordlist *toDel = found;
		found = found->next;
		delete[] toDel;
	}
}