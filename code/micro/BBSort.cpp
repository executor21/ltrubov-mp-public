#include "utils.h"

BBSort::BBSort(const char* numberFile){
	arr = loadDoubleFile(numberFile, &size);
	workCopy = (double*)malloc(size*sizeof(double));
}

void BBSort::workload(){
	for(long i = 0L; i < size; i++){
		workCopy[i] = arr[i];
	}
	double temp;
	for(long i = 0L; i < size-1; i++){
		for(long j = size-1; j > i; j--){
			if(workCopy[j] < workCopy[j-1]){
				temp = workCopy[j];
				workCopy[j] = workCopy[j-1];
				workCopy[j-1] = temp;
			}
		}
	}
}

BBSort::~BBSort(){
	delete[] arr;
	delete[] workCopy;
}
