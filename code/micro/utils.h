#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "MB.h"

int* loadIntegerFile(const char* fileName, long *n);
double* loadDoubleFile(const char* fileName, long *n);
double* loadDoubleFileAndTarget(const char* fileName, long *n, double *target);
double timeMark();
char* loadFileIntoCharArray(const char* fileName, long *n);
char** loadLinesOfFile(const char* fileName, int *n);

int customStrLen(const char* str);

#define BENCHMARK_COUNT 8

static const char* BM_NAMES[BENCHMARK_COUNT] = {"qs", "bbsort", "bsearch", 
		"minv", "mmul", "fib", "hash", "text"};

MB* benchmarkForIndex(int ind);