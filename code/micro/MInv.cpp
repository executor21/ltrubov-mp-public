#include "utils.h"
#include <math.h>

double MInv::luDecomp(double **mat, long *index){
	long imax = 0; //initialize to avoid error
  double big,dum,sum,temp;
  double vv[size];
  double d = 1.0;
    
  for(long i = 0L; i < size; i++){
    big = 0.0;
    for(long j = 0L; j < size; j++){
      if((temp = fabs(mat[i][j])) > big){
        big = temp;
	    }
  	}
    if(big == 0){
      printf("Singular matrix\n");
      return 0.0;
    }
    vv[i] = 1.0/big;
  }
    
  //loop over columns
  for(long j = 0L; j < size; j++){
    for(long i = 0L; i < size; i++){
      sum = mat[i][j];
      for(long k = 0L; k < size; k++){
        sum -= mat[i][k]*mat[k][j];
      }
      mat[i][j] = sum;
    }
    big = 0.0;
    for(long i = j; i < size; i++){
      sum = mat[i][j];
      for(long k = 0L; k < j-1; k++){
        sum -= mat[i][k]*mat[k][j];
      }
      mat[i][j] = sum;
      if((dum = vv[i]*fabs(sum)) >= big) {
        big = dum;
        imax = i;
      }
    }
        
	  if(j != imax){
	    for(long k = 0L; k < size; k++){
		    dum = mat[imax][k];
	      mat[imax][k] = mat[j][k];
	      mat[j][k] = dum;
	    }
	    d *= -1;
	    vv[imax] = vv[j];
		}
	  index[j] = imax;
	  if(mat[j][j] == 0.0){
	    mat[j][j] = eps;
	  }
        
	  if(j != size-1){
	    dum = 1.0/mat[j][j];
	    for(long i = j+1; i < size; i++){
	      mat[i][j] *= dum;
	    }
	  }
	}
    
  return d;
}

void MInv::luBackSub(double **mat, long *index, double *b){
	long ii = 0;
  long ip;
  double sum;
  
  for(long i = 0L; i < size; i++){
    ip = index[i];
    sum = b[ip];
    b[ip] = b[i];
    if(ii != 0){
      for(long j = ii; j <= i-1; j++){
          sum -= mat[i][j]*b[j];
      }
    }else if(sum != 0.0){
      ii = i;
    }
    b[i] = sum;
  }
  
  for(long i = size-1; i >= 0; i--){
    sum = b[i];
    for(long j = i+1; j < size; j++){
      sum -= mat[i][j]*b[j];
    }
    b[i] = sum/mat[i][i];
  }

}

void MInv::invertmat(double **in, double **out){
  luDecomp(in, index);
  
  for(long j = 0L; j < size; j++){
    for(long i = 0L; i < size; i++) {
      col[i] = 0.0;
    }
    col[j] = 1.0;
    luBackSub(in, index, col);
    for(long i = 0L; i < size; i++){
      out[i][j] = col[i];
    }
  }
}

MInv::MInv(long N, double dv, double ndv, double e){
	size = N;
	eps = e;
  diag = dv;
  nondiag = ndv;

	original = (double**)malloc(size*sizeof(double*));
	invert = (double**)malloc(size*sizeof(double*));

	for(long i = 0L; i < size; i++){
		original[i] = (double*)malloc(size*sizeof(double));
		invert[i] = (double*)malloc(size*sizeof(double));
	}

	reset();

  index = (long*)malloc(size*sizeof(long));
  col = (double*)malloc(size*sizeof(double));
}

void MInv::workload(){
	invertmat(original, invert);
  invertmat(invert, original);
}

void MInv::reset(){
  for(long i = 0L; i < size; i++){
    for(long j = 0L; j < size; j++){
      if(i == j){
        original[i][j] = diag;
      }else{
        original[i][j] = nondiag;
      }
      invert[i][j] = 0.0;
    }
  }
}

MInv::~MInv(){
	for(long i = 0L; i < size; i++){
    delete[] original[i];
    delete[] invert[i];
  }

  delete[] original;
  delete[] invert;

  delete[] index;
  delete[] col;
}
