#include "utils.h"
#include <string.h>

int* loadIntegerFile(const char* fileName, long *n){
	int* res = NULL;
	
	char str[16];
	FILE *f = fopen(fileName, "r");
	if(f){
		if(fscanf(f, "%s", str) != EOF){
			long count = atol(str);
			*n = count;
			if(n > 0){
				res = (int*)malloc(count*sizeof(int));
				long i = 0L;
				while(fscanf(f, "%s", str) != EOF && i < count){
					res[i++] = atoi(str);
				}
			}
		}
		fclose(f);
	}
	
	return res;
}

double* loadDoubleFile(const char* fileName, long *n){
	double* res = NULL;
	
	char str[32];
	FILE *f = fopen(fileName, "r");
	if(f){
		if(fscanf(f, "%s", str) != EOF){
			long count = atol(str);
			*n = count;
			if(n > 0){
				res = (double*)malloc(count*sizeof(double));
				long i = 0L;
				while(fscanf(f, "%s", str) != EOF && i < count){
					res[i++] = atof(str);
				}
			}
		}
		fclose(f);
	}
	
	return res;
}

double* loadDoubleFileAndTarget(const char* fileName, long *n, double *target){
	double* res = NULL;
	
	char str[32];
	FILE *f = fopen(fileName, "r");
	if(f){
		if(fscanf(f, "%s", str) != EOF){
			long count = atol(str);
			*n = count;

			if(fscanf(f, "%s", str) != EOF){
				double t = atof(str);
				*target = t;


				if(n > 0){
					res = (double*)malloc(count*sizeof(double));
					long i = 0L;
					while(fscanf(f, "%s", str) != EOF && i < count){
						res[i++] = atof(str);
					}
				}
			}
		}
		fclose(f);
	}
	
	return res;
}

double timeMark(){
	return ((double)(clock()))/CLOCKS_PER_SEC;
}

char* loadFileIntoCharArray(const char* fileName, long *n){
	FILE *f;
	long fileSize;
	char *content = NULL;

	//*
	f = fopen (fileName, "r");
	if(!f) {
		printf("error with file %s\n", fileName);
		return NULL;
	}

	fseek(f, 0L, SEEK_END);
	fileSize = ftell(f);
	rewind(f);

	//printf("file is %ld bytes long\n", fileSize);

	//content = (char*)malloc(fileSize+1);
	//content = (char*)malloc(100000*sizeof(double));

	content = (char*)calloc(1, fileSize+1);
	
	//*
	if(!content ){
		fclose(f);
		printf("failed to allocate memory\n");
		return NULL;
	}

	if(1!=fread(content, fileSize, 1 , f)){
	  fclose(f);
	  free(content);
	  printf("failed to read file content\n");
	  return NULL;
	}//*/

	fclose(f);

	*n = fileSize;

	return content;
}

char** loadLinesOfFile(const char* fileName, int *n){
	char** res = NULL;
	
	char str[256];

	FILE *f = fopen(fileName, "r");
	if(f){
		if(fscanf(f, "%s", str) != EOF){
			int count = atoi(str);
			*n = count;
			if(n > 0){
				res = (char**)malloc(count*sizeof(char*));
				long i = 0L;
				while(fscanf(f, "%s", str) != EOF && i < count){
					char* store = (char*)malloc((strlen(str)+1)*sizeof(char));

					//printf("%s ", str);
					strcpy(store, str);
					res[i++] = store;
					//printf("%s\n", res[i-1]);
				}
			}
		}
		fclose(f);
	}
	
	return res;
}

int customStrLen(const char* str){
	int len = 0;
	while(*str){
		len++;
		*str++;
	}
	return len;
}

MB* benchmarkForIndex(int ind){
	switch(ind){
		case 0: return new QS("qs.dat");
		case 1: return new BBSort("bbsort.dat");
		case 2: return new BSearch("bsearch.dat");
		case 3: return new MInv(200L, 2.001, 1.001, 0.0000001);
		case 4: return new MMul(200, -100, 100);
		case 5: return new Fib(35);
		case 6: return new Enc("hamlet.html");
		case 7: return new Text("words.txt", "cal");
		default:
			return NULL;
	}
}
