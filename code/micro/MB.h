
class MB{

	public:
	virtual void workload() = 0;
	virtual void reset() {}
	virtual ~MB() {}
};

class QS : public MB {
private:
	int *arr;
	long size;
	int *workCopy;

	void swapArrayValues(long i, long j);
	long partitionArray(long lo, long hi);
	void quicksortArray(long lo, long hi);

public:
	QS(const char* numberFile);
	virtual void workload();
	virtual ~QS();
	
	//int valueAt(int index);

};

class BBSort : public MB {
private:
	double *arr;
	long size;
	double *workCopy;

public:
	BBSort(const char* numberFile);
	virtual void workload();
	virtual ~BBSort();

};

class BSearch : public MB{
	private:
	double *arr;
	long size;
	double target;
	long result;

public:
	BSearch(const char* numberFile);
	virtual void workload();
	virtual ~BSearch();


};

class MInv : public MB{
private:
	double **original;
	double **invert;
	long size;
	double eps;
	long *index;
  double *col;

  double diag;
  double nondiag;

	double luDecomp(double **mat, long *index);
	void luBackSub(double **mat, long *index, double *b);
	void invertmat(double **in, double **out);



public:
	MInv(long N, double dv, double ndv, double e);
	virtual void workload();
	virtual void reset();
	virtual ~MInv();
};

class MMul : public MB {
private:
	int* matA;
	int* matB;
	int* matC;
	long size;

public:
	MMul(long N, int lv, int hv);
	virtual void workload();
	virtual ~MMul();

};

class Fib : public MB {
private:
	int target;
	int result;

	int fib(int n);

public:
	Fib(int t);
	virtual void workload();
	virtual ~Fib();

};

class Enc : public MB {
private:
	unsigned char *data;
	long size;
	unsigned char *result;

public:
	Enc(const char* fileName);
	virtual void workload();
	virtual ~Enc();
};

struct Wordlist {
	char* word;
	Wordlist *next;
};

class Text : public MB {
private:
	char** words;
	int size;

	const char* tag;
	int count;
	Wordlist *found;

public:
	Text(const char* fileName, const char* searchTag);
	virtual void workload();
	//virtual void reset();
	virtual ~Text();
};


