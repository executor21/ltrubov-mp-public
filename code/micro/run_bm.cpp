//Executes a single test program's workload() function
//a set number of times
//Run as follows: $(EXEC_NAME) $(TEST_PROG) $(RUN_COUNT)
//The run count parameter is optional, default is 1
//If the underlying program has been injected with the DIA
//pass, virtual instruction tallies will be printed to
//standard output

#include "utils.h"
#include <string.h>

extern const char* BM_NAMES[];

int main(int argc, char *argv[]){
	//program name required
	if(argc < 2){
		printf("Benchmark argument required. Exiting program.\n");
		return 0;
	}

	int bmToUse = -1;
	for(int i = 0; i < BENCHMARK_COUNT; i++){
		if(strcmp(argv[1], BM_NAMES[i]) == 0){
			bmToUse = i;
			break;
		}
	}

	//must be one of existing test programs
	if(bmToUse < 0){
		printf("Benchmark argument invalid. Exiting program.\n");
		return 0;
	}

	//number of times to run the workload() function
	int wkldCount = 1;
	if(argc >= 3){
		wkldCount = atoi(argv[2]);
	}

	MB* mb = benchmarkForIndex(bmToUse);

	for(int i = 0; i < wkldCount; i++){
		mb->workload();
	}
	
	delete mb;
}
