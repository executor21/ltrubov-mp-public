#!/usr/bin/env ruby2.0

#require File.join(File.dirname(__FILE__), 'cust_rand')

outFile = "qs.dat"
min = -10000
max = 10000
seed = 33550336

N = 1000000
if ARGV[0] && ARGV[0].to_i > 0
	N = ARGV[0].to_i
end

rand = Random.new(seed)
f = File.new(File.join(File.dirname(__FILE__), outFile), 'w')
f.write("#{N.to_s}\n")
N.times do
	nr = rand.rand(min..max)
	f.write("#{nr.to_s}\n")
end

f.close
