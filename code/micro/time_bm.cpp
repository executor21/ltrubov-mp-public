#include "utils.h"
#include <string.h>

#define TIMING_ITERATIONS 10

extern const char* BM_NAMES[];

int main(int argc, char *argv[]){

	double speeds[BENCHMARK_COUNT] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

	double t1, t2, t3;

	int factor;

	MB* curr = NULL;

	for(int i = 0; i < BENCHMARK_COUNT; i++){
		if(i == 2){
			factor = 10000;
		}else{
			factor = 1;
		}

		curr = benchmarkForIndex(i);

		t1 = timeMark();
		for(int j = 0; j < TIMING_ITERATIONS*factor; j++){
			curr->workload();
			curr->reset();
		}
		t2 = timeMark();
		for(int j = 0; j < TIMING_ITERATIONS*factor; j++){
			curr->workload();
			curr->workload();
			curr->reset();
		}
		t3 = timeMark();

		speeds[i] = (t3 - 2*t2 + t1)/(TIMING_ITERATIONS*factor);

		delete curr;

	}

	for(int i = 0; i < BENCHMARK_COUNT; i++){
		if(speeds[i] != 0.0){
			printf("%s: %.17g seconds\n", BM_NAMES[i], speeds[i]);
		}
	}
}
