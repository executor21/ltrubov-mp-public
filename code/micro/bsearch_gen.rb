#!/usr/bin/env ruby

#require File.join(File.dirname(__FILE__), 'cust_rand')

outFile = "bsearch.dat"
min = -10000.0
max = 10000.0
seed = 33550336

N = 1000000

rand = Random.new(seed)
f = File.new(File.join(File.dirname(__FILE__), outFile), 'w')
f.write("#{N.to_s}\n")
store = []
N.times do
	nr = (max - min) * rand.rand + min
	store.push(nr)
end

tar = store[rand.rand(0...N)]

store.sort!

f.write("#{tar.to_s}\n")

store.each do |nr|
	f.write("#{nr.to_s}\n")
end

f.close
