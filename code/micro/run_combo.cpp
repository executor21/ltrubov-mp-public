//#include "MB.h"
#include "utils.h"
#include <string.h>

extern const char* BM_NAMES[];

int main(int argc, char *argv[]){
	if(argc < 4){
		printf("Benchmark arguments required. Exiting program.\n");
		return 0;
	}

	int bmsToUse[] = {-1,-1,-1};
	for(int j = 0; j < 3; j++){
		for(int i = 0; i < BENCHMARK_COUNT; i++){
			if(strcmp(argv[j+1], BM_NAMES[i]) == 0){
				bmsToUse[j] = i;
				break;
			}
		}
	}

	for(int i = 0; i < 3; i++){
		if(bmsToUse[i] < 0){
			printf("Benchmark argument invalud. Exiting program.\n");
			return 0;
		}
	}

	

	int wkldCount = 1;
	if(argc > 4){
		wkldCount = atoi(argv[4]);
	}

	/*
	MB** mbs = new MB*[6];
	mbs[0] = new QS("qs.dat");
	mbs[1] = new BBSort("bbsort.dat");
	mbs[2] = new BSearch("bsearch.dat");
	mbs[3] = new MInv(200L, 2.001, 1.001, 0.0000001);
	mbs[4] = new MMul(200, -100, 100);
	mbs[5] = new Fib(35);//*/


	//MB** mbs = new MB*[3];

	MB* mb1 = benchmarkForIndex(bmsToUse[0]);
	MB* mb2 = benchmarkForIndex(bmsToUse[1]);
	MB* mb3 = benchmarkForIndex(bmsToUse[2]);
	/*
	for(int i = 0; i < 3; i++){
		mbs[i] = benchmarkForIndex(bmsToUse[i]);
	}//*/

	for(int i = 0; i < wkldCount; i++){
		mb1->workload();
		mb2->workload();
		mb3->workload();
	}
	
	delete mb1;
	delete mb2;
	delete mb3;

	/*
	delete mbs[0];
	delete mbs[1];
	delete mbs[2];//*/
	
	//delete [] mbs;//*/
}
