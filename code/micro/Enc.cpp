#include "utils.h"
#include "sha/sha.h"

Enc::Enc(const char* fileName){
	data = (unsigned char*)loadFileIntoCharArray(fileName, &size);
	result = (unsigned char*)malloc(SHA256_DIGEST_LENGTH+1);
}

void Enc::workload(){
	SHA256(data, size, result);
}

Enc::~Enc(){
	/*
	for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
    printf("%02x", result[i]);
  }
  printf("\n");//*/
	free(data);
}