#!/usr/bin/env ruby

#require File.join(File.dirname(__FILE__), 'cust_rand')

outFile = "bbsort.dat"
min = -10000.0
max = 10000.0
seed = 33550336

N = 10000
if ARGV[0] && ARGV[0].to_i > 0
	N = ARGV[0].to_i
end

rand = Random.new(seed)
f = File.new(File.join(File.dirname(__FILE__), outFile), 'w')
f.write("#{N.to_s}\n")
N.times do
	nr = (max - min) * rand.rand + min
	f.write("#{nr.to_s}\n")
end

f.close
