#include <stdio.h>

long instructionStats[128];

void output() __attribute__ ((destructor));

void zeroStats(){
  for(int i = 0; i < 128; i++){
    instructionStats[i] = 0L;
  }
}

void update(unsigned ind, long increase){
  instructionStats[ind] += increase;
}

void output(){
	long total = 0;
	long curr;
	printf("BEGIN_INSTRUCTION_BREAKDOWN\n");
	for(int i = 0; i < 128; i++){
		curr = instructionStats[i];
		if(curr == 0) { 
		  continue;
		}
		printf("%d : %ld\n", i, curr);
		total += curr;
	}
	printf("END_INSTRUCTION_BREAKDOWN\n");
  printf("%ld instructions executed\n", total);
}
