//===- DIA.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"

using namespace llvm;

#define DEBUG_TYPE "dia"

namespace {
  struct DIA : public ModulePass {
    static char ID; // Pass identification, replacement for typeid

    DIA() : ModulePass(ID) {}

    bool doInitialization(Module &M) override {

      Function *main = M.getFunction("main");
      Function *rep = M.getFunction("_Z6outputv");
      Function *upd = M.getFunction("_Z6updatejl");
      Function *zer = M.getFunction("_Z9zeroStatsv");


      IntegerType *intType = Type::getInt32Ty(M.getContext());
      IntegerType *longType = Type::getInt64Ty(M.getContext());
      
      long stats[128];

      for(Module::iterator i = M.begin(), e = M.end(); i != e; ++i){
        if (i->getName().equals(rep->getName()) ||
            i->getName().equals(upd->getName()) ||
            i->getName().equals(zer->getName()) ) { 
          //do not add counters to output function or update functions
          continue;
        }
        
        for(Function::iterator j = i->begin(), f = i->end(); j != f; ++j){
          //at start of each basic block, zero out
          zeroLongArray(stats, 128);
          for(BasicBlock::iterator k = j->begin(), g = j->end(); k != g; ++k){
            stats[k->getOpcode()]++;
          }
          
          TerminatorInst *term = j->getTerminator(); 
          
          for(int i = 0; i < 128; i++){
            if(stats[i] != 0){
              Value *ind = ConstantInt::get(intType, i);
              Value *amt = ConstantInt::get(longType, stats[i]);
              ArrayRef <Value*> *args = new ArrayRef<Value*>({ind, amt});
              
              CallInst::Create(upd, *args, "", term);
              
            }
          }
        }
      }
      
      BasicBlock *firstBlock = &main->front();
      Instruction *firstInst = &(*(firstBlock->getFirstInsertionPt()));
      
      CallInst::Create(zer, "", firstInst);

      //BasicBlock *lastBlock = &main->back();
 
      //TerminatorInst *lastInst = lastBlock->getTerminator(); 

      //CallInst::Create(rep, "", lastInst);

      return true;
    }

    bool runOnModule(Module &M) override {
      return false;
    }

    void zeroLongArray(long arr[], int n){
      for(int i = 0; i < n; i++){
        arr[i] = 0L;
      } 

    } 
  };
}

char DIA::ID = 0;
static RegisterPass<DIA> X("dia", "Dynamic Instruction Breakdown Pass");
