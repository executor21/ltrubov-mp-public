#!/usr/bin/env ruby2.0

require_relative 'benchmark'
require_relative 'measure'
require_relative 'file_ops'
require_relative 'grouping'
require_relative 'utils'
require_relative 'float_extra'
require_relative 'data_storage'
require_relative 'tex'


#---------script body----------------------------------------

unless ARGV.length >= 1
  puts "Invalid argument number"
  exit
end

met_type = "llvm-ir"
progs = FileOps.load_benchmark_programs(ARGV[0])


out = Tex.texTableHeader(progs.length+1)
titles = ["\\textbf{Program}"]
progs.each do |i|
  titles.push(i.name)
end
out += Tex.texTableLine(titles, true)
out += Tex.textLineSeparator

progs.each do |i|
  line = [i.name]
  progs.each do |j|
    line.push(i.distance_from_prog(j, met_type))
  end
  out += Tex.texTableLine(line)
  out += Tex.textLineSeparator
end

out += Tex.texTableFooter
puts out

