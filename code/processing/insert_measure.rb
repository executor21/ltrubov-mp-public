#!/usr/bin/env ruby2.0

require_relative 'benchmark'
require_relative 'measure'
require_relative 'file_ops'
require 'json'

#functions




#---------script body----------------------------------------

unless ARGV.length >= 1
  puts "Invalid argument number"
  exit
end

inputLines = FileOps.lines_of_file(ARGV[0])
progName=inputLines[0]

m,d = FileOps.process_data_file(inputLines)

m = m.sub('-raw','') #might be present in outfile, shouldn't be in json

#puts m
#puts d
#puts inputLines


progs = ARGV[1] ? 
	FileOps.load_benchmark_programs(ARGV[1]) :
	FileOps.load_benchmark_programs('breakdown.json')

targetProg = Benchmark.benchmark_named_or_new(progs, progName)
unless targetProg.has_measure_of_type(m)
	msr = Measure.new(vals: d, type: m)
  targetProg.measures.push(msr)
end

#puts targetProg.hasMeasureOfType(m)

ARGV[1] ?
	FileOps.save_benchmark_programs(progs, ARGV[1]) :
	FileOps.save_benchmark_programs(progs, 'breakdown.json')


