#!/usr/local/bin/ruby2.0

require_relative 'utils'
require_relative 'file_ops'


#data -- obtained from experiments

MEAN_TIMES = [
    [nil] #dummy for 1-based indexing
]

def program_times_array(prog_name)
  MEAN_TIMES.each do |e|
    unless e[0] && e[0] == prog_name
      next
    end
    return e
  end
  ne = [prog_name]
  MEAN_TIMES.push(ne)
  ne
end

def add_to_mean_times(raw, index)
  MEAN_TIMES[0].push(index)
  raw.each do |k,v|
    pta = program_times_array(k)
    pta.push(v.reduce(:+)/v.length)
  end

end

def process_file_lines(lines, index)
  raw = {}
  lines.each do |l|
    if l.empty? || l.include?("iteration")
      next
    end

    linecomps = l.split(" ")
    prog = linecomps[0]
    time = linecomps[1].to_f

    if time > 0
      raw[prog] ||= []
      raw[prog].push(time)
    end

  end


  add_to_mean_times(raw,index)

end

#---------script body----------------------------------------

unless ARGV.length >= 1
  puts "Invalid argument number, data files required"
  exit
end

ARGV.each_with_index do |f,i|
  fl = FileOps::lines_of_file(f)
  process_file_lines(fl,i+1)
end

MEAN_TIMES.each do |mt|
  puts mt.to_s
end

