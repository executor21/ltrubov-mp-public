#This class contains common functions frequently required for
#this project, such as normalization and operations on program
#characterization vectors

class Utils

  #input: h, a dictionary where the values are numeric
  #returns a hash with the same keys, but the values
  #normalized so that their sum is 1
  def self.normalize_hash(h)
    total = 0
    h.each do |_,v|
      total += v
    end

    answer = {}
    h.each do |k,v|
      answer[k] = (v.to_f/total)
    end

    answer
  end

  #input: h1 and h2, two dictionaries where the values are numeric
  #returns a hash with the same keys, but the values being sums of the
  #corresponding values in the two hashes
  #if a key is not present in one of the hashes, it is assumed to have
  #value of 0
  def self.hash_sum(h1,h2)
    ans = {}
    keys = (h1.keys + h2.keys).uniq
    keys.each do |k|
      sum = (h1[k] ||= 0) + (h2[k] ||= 0)
      ans[k] = sum
    end
    ans
  end

  #input: h1 and h2, two dictionaries where the values are numeric
  #returns a hash with the same keys, but the values being absolute
  #differences of the corresponding values in the two hashes
  #if a key is not present in one of the hashes, it is assumed to have
  #value of 0
  def self.abs_hash_difference(h1, h2)
    ans = {}
    keys = (h1.keys + h2.keys).uniq
    keys.each do |k|
      diff = (h1[k] ||= 0) - (h2[k] ||= 0)
      ans[k] = diff.abs
    end

    ans
  end

  #input: h1 and h2, two dictionaries where the values are numeric
  #returns the Manhattan distance between their normalized versions
  def self.percent_distance(h1, h2)
    n1 = normalize_hash(h1)
    n2 = normalize_hash(h2)

    dh = abs_hash_difference(n1, n2)
    0.5*(dh.values.reduce(:+))
  end

  #input: h, a dictionary where the values are numeric
  #       f, a numeric constant
  #returns a hash with the same keys, but the values multiplied by f
  def self.hash_product(h, f)
    ans = {}
    h.keys.each do |k|
      ans[k] = (h[k])*f
    end
    ans
  end

  #input: a hash
  #prints the hash in form of KEY : VALUE with each key and value
  #on an individual line
  #prints to standard output
  def self.neat_print_hash(h)
  	h.each do |k,v|
  		unless v == 0
	  		puts "#{k} : #{v.to_s}"
	  	end
  	end
  end

  #Gets all possible values of w1, w2, w3 such that
  #w1 + w2 + w3 = 1, with the granularity of step
  def self.weight_range(step, digits=4)
    res = []
    (0..1).step(step) do |w1|
      (0..(1-w1)).step(step) do |w2|
        w3 = 1-w1-w2
        res.push([w1.round(digits), w2.round(digits), w3.round(digits)])
      end
    end

    res

  end
end
