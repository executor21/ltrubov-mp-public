#!/usr/local/bin/ruby2.0

require_relative '../data_storage'
require_relative '../float_extra'

Decomp = Struct.new(:target, :micros, :systems, :weights, :errorX)
InsDecomp = Struct.new(:target, :micros, :weights, :error)

def timing_ratio_error(tar,c,s,w)
  _T = DataStorage::MACRO_TIMES
  real_ratio = _T[tar][s[0]]/_T[tar][s[1]]
  t = DataStorage::MICRO_TIMES
  r = [t[c[0]][s[0]]/t[c[0]][s[1]],
       t[c[1]][s[0]]/t[c[1]][s[1]],
       t[c[2]][s[0]]/t[c[2]][s[1]]]

  pred_ratio = (r[0]**w[0])*(r[1]**w[1])*(r[2]**w[2])

  (real_ratio - pred_ratio).abs/real_ratio
end

def find_decomp(data,tar,mbs,sys=nil)
  data.each do |d|
    if d.target == tar && d.micros == mbs
      if sys
        if sys == d.systems
          return d
        end
      else
        return d
      end
    end
  end
  nil
end



#---------script body----------------------------------------

unless ARGV.length >= 2
  puts "Invalid argument number"
  exit
end

micros = %w(bsearch qs mmul bbsort fib)
micros_num = [1,2,3,4,5]
micro_combos = micros_num.combination(3)


targets = %w(minv hash text)
targets_num = [1,2,3]

systems = [1,2,3,4]
sys_combos = systems.combination(2)

dec1 = Marshal.load(File.read(ARGV[0]))
dec2 = Marshal.load(File.read(ARGV[1]))

targets_num.each do |tar|
  micro_combos.each do |mc|
    sys_combos.each do |sc|
      d1 = find_decomp(dec1,tar,mc,sc)

      mics = [
          micros[mc[0]-1],
          micros[mc[1]-1],
          micros[mc[2]-1]
      ]

      d2 = find_decomp(dec2, targets[tar-1], mics)

      e1 = d1[:errorX]
      puts "#{tar} #{mc}" if d2 == nil
      e2 = timing_ratio_error(tar,mc,sc,d2[:weights])

      puts "#{e1.to_4s}, #{e2.to_4s}, #{targets[tar-1]}\\\\"

    end
  end
end