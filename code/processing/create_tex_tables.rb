#!/usr/bin/env ruby2.0

require_relative 'benchmark'
require_relative 'measure'
require_relative 'file_ops'
require 'json'

#functions




#---------script body----------------------------------------

targets = ARGV
#targets = %w(bbsort)
targets = %w(bbsort bsearch fib minv mmul qs)

progs = FileOps.load_benchmark_programs('breakdown.json')

targets.each do |t|
  tp = Benchmark.benchmark_named(progs, t)
  unless tp
    next
  end

  puts t
  puts tp.tex_chart_representation
  puts "\n"

end

