class InstCounterparts
  def self.counterpartsData
    {
        "ret" => %w(RET_NEAR),
        "br" => %w(JBE JL JLE JNLE JNZ JMP JNL JP JZ JNBE JNP),
        "add" => %w(ADD INC),
        "load" => %w(),
        "store" => %w(),
        "getelementptr" => %w(),
        "bitcast" => %w(),
        "icmp" => %w(CMP),
        "fcmp" => %w(UCOMISD CMPSD_XMM),
        "phi" => %w(),
        "call" => %w(CALL_NEAR),
        "sdiv" => %w(),
        "fsub" => %w(SUBSD),
        "fmul" => %w(MULSD),
        "fdiv" => %w(DIVSD),
        "alloca" => %w(),
        "select" => %w(),
        "mul" => %w(IMUL),
        "shl" => %w(SHL)
    }
  end
end