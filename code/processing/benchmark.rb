

require_relative 'measure'
require_relative 'utils'
require_relative 'tex'
require_relative 'inst_counterparts'

require_relative 'float_extra'



class Benchmark
	def self.benchmark_named(progs, prog_name)
		progs.select { |p| p.name == prog_name}.first
	end

	def self.benchmark_named_or_new(progs, prog_name)
		existing_prog = self.benchmark_named(progs, prog_name)
		unless existing_prog
      existing_prog = Benchmark.new(name: prog_name)
			progs.push(existing_prog)
		end
    existing_prog
	end

	attr_reader :name
	attr_reader :measures

  # @param [Benchmark] name
  def initialize(name: 'program')
    @name = name
    @measures = []
	end

	def measure_of_type(measure_type)
		@measures.select { |m| m.type == measure_type}.first
	end

	def has_measure_of_type(measure_type)
		 self.measure_of_type(measure_type) != nil
	end
  
  def data
  	res = {}
  	@measures.each do |v|
  		res[v.type] = v.vals
  	end
  	
  	res
	end

	def grouped_data(grouping, type)
		unless self.has_measure_of_type(type)
			return nil
		end

		msr = self.measure_of_type(type)

		msr.grouped_data(grouping)
  end

  def self.combo_prog(progs,weights,type='llvm-ir',level=2)
    grp = Grouping.groupingForMeasureType(type, level)
    res_hash = {}
    progs.each_with_index do |p,i|
      #puts weights
      #puts "#{p} #{weights[i]}"
      w_hash = Utils.hash_product(p.grouped_data(grp, type), weights[i])

      #puts w_hash.class
      res_hash = Utils.hash_sum(res_hash,w_hash)
    end

    #puts res_hash.to_s
    #exit

    Utils.normalize_hash(res_hash)
  end

	def distance_from_prog(other_prog, type='llvm-ir', level=2)
		grp = Grouping.groupingForMeasureType(type, level)
		my_data = self.grouped_data(grp, type)
		op_data = other_prog.grouped_data(grp, type)

		unless my_data && op_data
			return -1.0
		end

		Utils.percent_distance(my_data, op_data)
	end

	def tex_chart_representation
    res = Tex.texTableHeader(5)

    intel_m = self.measure_of_type('intel').vals
    llvm_m = self.measure_of_type('llvm-ir').vals

    cpts = InstCounterparts.counterpartsData

    vals_noted = []

    llvm_m.each do |k,v|
      rel_intel = intel_m.select {|key| (cpts[k]).include?(key) }

      if rel_intel.empty?
        next
      elsif rel_intel.length == 1
        c = rel_intel.keys.first
        iv = rel_intel[c]
        res += Tex.texTableLine([k,v,c,iv,(v-iv).abs])
        res += Tex.textLineSeparator
        vals_noted.push(c)
      else
        multirow_lines = []

        mlr = [
              "\\multirow{#{rel_intel.length}}{*}{#{k}",
              "\\multirow{#{rel_intel.length}}{*}{#{v.to_s}}",
              '',
              '',
              ''
            ]

        tv = 0

        rel_intel.each do |kk, vv|


          mlr[2] = kk
          mlr[3] = vv


          tv += vv
          vals_noted.push(kk)

          multirow_lines.push(mlr.clone)
          mlr = ['','','','','']
        end

        multirow_lines.first[-1] = "\\multirow{#{rel_intel.length}}{*}{#{diff.abs.to_s}}"
        multirow_lines.each do |m|
          res += Tex.texTableLine(m)
        end
        res += Tex.textLineSeparator


      end

    end

    llvm_m.each do |k,v|
      rel_intel = intel_m.select {|key| (cpts[k]).include?(key) }

      unless rel_intel.empty?
        next
      end

      res += Tex.texTableLine([k,v,'','',''])
    end

    intel_m.each do |k,v|
      if vals_noted.include?(k)
        next
      end

      res += Tex.texTableLine(['','',k,v,''])
    end


    res + Tex.texTableFooter
	end
end
