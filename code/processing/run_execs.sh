#!/bin/bash

cd -- "$(dirname "$0")"

res="results.txt"

if [ -f micro.ir ] ; then
	mbs=( "bbsort" "bsearch" "fib" "minv" "mmul" "qs" ) 
	for m in "${mbs[@]}"
	do
		echo $m >> $res
		echo "llvm-raw" >> $res
		eval ./micro.ir $m 1 >> $res
		echo "" >> $res

		echo $m >> $res
		echo "llvm-raw" >> $res
		eval ./micro.ir $m 2 >> $res
		echo "" >> $res
	done
fi

if [[ -f gzip.ir && -f shakespeare.tar ]] ; then
	cp shakespeare.tar shakespeare1.tar
	echo "gzip" >> $res
	echo "llvm-ir" >> $res
	eval ./gzip.ir shakespeare1.tar >> $res
	echo "" >> res
fi

for i in {1..10}
do
	echo "iteration $i" >> $res



	if [[ -f micro.time.exe && -f qs.dat && -f bbsort.dat && -f bsearch.dat ]] ; then
		eval ./micro.time.exe >> $res
		echo "" >> $res
	fi

	if [[ -f gzip && -f macro.time.exe && -f shakespeare.tar && -f sample.cc ]] ; then
		eval ./macro.time.exe >> $res
		echo "" >> $res
	fi

done


#echo "" >> $res

