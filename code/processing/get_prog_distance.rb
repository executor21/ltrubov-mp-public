#!/usr/local/bin/ruby2.0

require_relative 'benchmark'
require_relative 'measure'
require_relative 'file_ops'
require_relative 'grouping'
require_relative 'utils'

$decomps = []
$must_recompute = true



InsDecomp = Struct.new(:target, :micros, :weights, :error)

def find_decomp(tar, mbs)
  $decomps.each do |d|
    if d.target == tar && d.micros == mbs
      return d
    end
  end
  nil
end

#---------script body----------------------------------------

unless ARGV.length >= 1
  puts "Invalid argument number"
  exit
end

data_file = ARGV[0]

micros = %w(bsearch qs mmul bbsort fib)
micro_combos = micros.combination(3)

targets = %w(minv hash text)

grouping_level = ARGV[2] ? ARGV[2].to_i : 2

if ARGV[1] && File.exist?(ARGV[1])
  $decomps = Marshal.load(File.read(ARGV[1]))
  if $decomps.length > 0
    $must_recompute = false
  end
end

if $must_recompute

  progs = FileOps.load_benchmark_programs(data_file)

  met="llvm-ir"
  gr = Grouping.groupingForMeasureType(met,grouping_level)

  gd = {}
  (micros+targets).each do |t|
    p = Benchmark.benchmark_named(progs, t)
    gd[t] = Utils.normalize_hash(p.grouped_data(gr, met))
  end

  wr = Utils.weight_range(0.001)

  targets.each do |tar|
    puts "#{tar}:"
    micro_combos.each do |mc|
      d = InsDecomp.new(tar,mc)
      curr_progs = [
          Benchmark.benchmark_named(progs, mc[0]),
          Benchmark.benchmark_named(progs, mc[1]),
          Benchmark.benchmark_named(progs, mc[2])
      ]

      err = Float::MAX
      w_best = [0.0,0.0,0.0]
      wr.each do |w|
        cp = Benchmark.combo_prog(curr_progs,w,met,grouping_level)
        e_curr = Utils.percent_distance(cp, gd[tar])
        if e_curr < err
          w_best = w
          err = e_curr
        end
      end
      d[:weights] = w_best
      d[:error] = err
      $decomps.push(d)
    end
  end

  out_file = ARGV[1] ? ARGV[1] : "insmix.decomp.rbobj"
  File.open(out_file, 'w') {|f| f.write(Marshal.dump($decomps)) }
end

targets.each do |tar|
  puts "#{tar}:"
  micro_combos.each do |mc|
    dec = find_decomp(tar,mc)
    puts "#{mc}: #{dec.weights} #{dec.error}"
  end
end

puts ""

targets.each do |tar|
  out = Tex.texTableHeader(3)
  titles = [
      "\\textbf{\\shortstack{Decomposition\\\\Components}}",
      "\\textbf{Weights}",
      "\\textbf{Distance}"
  ]
  out += Tex.texTableLine(titles, true)
  out += Tex.textLineSeparator
  micro_combos.each do |mc|
    i1 = micros.index(mc[0]) + 1
    i2 = micros.index(mc[1]) + 1
    i3 = micros.index(mc[2]) + 1
    line = ["${b_#{i1},b_#{i2},b_#{i3}}$"]

    dRel = find_decomp(tar,mc)
    if dRel
      w = dRel[:weights]
      line.push("\\shortstack{#{w[0].to_4s},\\\\#{w[1].to_4s},\\\\#{w[2].to_4s}}")
      line.push(dRel[:error])
    end
    out += Tex.texTableLine(line, true)
    out += Tex.textLineSeparator
  end

  out += Tex.texTableFooter
  puts out
  puts ""
end

puts "------"

out = Tex.texTableHeader(7)
titles = ["\\textbf{\\shortstack{Decomposition\\\\Targets\\/ \
            \\\\Decomposition\\\\Components}}"]

targets.each do |tar|
  i = targets.index(tar) + 1
  titles.push("\\multicolumn{2}{|c|}{${B_#{i}}$}")
end

out += Tex.texTableLine(titles, true)
out += Tex.textLineSeparator

micro_combos.each do |mc|
  i1 = micros.index(mc[0]) + 1
  i2 = micros.index(mc[1]) + 1
  i3 = micros.index(mc[2]) + 1
  line = ["${b_#{i1},b_#{i2},b_#{i3}}$"]
  targets.each do |tar|
    dRel = find_decomp(tar,mc)
    if dRel
      w = dRel[:weights]
      line.push("\\shortstack{#{w[0].to_4s},\\\\#{w[1].to_4s},\\\\#{w[2].to_4s}}")
      line.push(dRel[:error])
    end
  end
  out += Tex.texTableLine(line, true)
  out += Tex.textLineSeparator
end

out += Tex.texTableFooter
puts out


