class Grouping
	def self.LLVMOpcodeDictionary
		{
      "0" => '<Invalid operator>',
      "1" => "ret",
      "2" => "br",
			"3" => "switch",
			"4" => "indirectbr",
			"5" => "invoke",
			"6" => "resume",
			"7" => "unreachable",
			"8" => "cleanupret",
			"9" => "catchret",
			"10" => "catchswitch",
			"11" => "add",
			"12" => "fadd",
			"13" => "sub",
			"14" => "fsub",
			"15" => "mul",
			"16" => "fmul",
			"17" => "udiv",
			"18" => "sdiv",
			"19" => "fdiv",
			"20" => "urem",
			"21" => "srem",
			"22" => "frem",
			"23" => "shl",
			"24" => "lshr",
			"25" => "ashr",
			"26" => "and",
			"27" => "or",
			"28" => "xor",
			"29" => "alloca",
			"30" => "load",
			"31" => "store",
			"32" => "getelementptr",
			"33" => "fence",
			"34" => "cmpxchg",
			"35" => "atomicrmw",
			"36" => "trunc",
			"37" => "zext",
			"38" => "sext",
			"39" => "fptoui",
			"40" => "fptosi",
			"41" => "uitofp",
			"42" => "sitofp",
			"43" => "fptrunc",
			"44" => "fpext",
			"45" => "ptrtoint",
			"46" => "inttoptr",
			"47" => "bitcast",
			"48" => "addrspacecast",
			"49" => "cleanuppad",
			"50" => "catchpad",
			"51" => "icmp",
			"52" => "fcmp",
			"53" => "phi",
			"54" => "call",
			"55" => "select",
			"58" => "va_arg",
			"59" => "extractelement",
			"60" => "insertelement",
			"61" => "shufflevector",
			"62" => "extractvalue",
			"63" => "insertvalue",
			"64" => "landingpad"
    }
	end

	attr_reader :groupMap

	def initialize(gm = {})
		@groupMap = gm
	end

	def groupForOpcode(oc)
		@groupMap.each do |k,v|
			#puts "#{oc} #{v.join(" ")}"
			if v.include?(oc)
				return k
			end
		end

		nil
	end

  def manualGroupingKeys
		%w(INT_ADD_SUB INT_MUL_DIV FLOAT_ADD_SUB FLOAT_MUL_DIV FUNCALL COMPARE LOAD STORE)
	end

	def self.defaultGroupingKeys
		%w(CTRL_PLAIN CTRL_ADV INT_SUM INT_PROD INT_CMP FLOAT_SUM) +
				%w(FLOAT_PROD FLOAT_CMP LOGIC CONV CAST SHIFT MISC_OP) +
				%w(LOAD STORE MOVE OTHER)
	end

  def self.coarseGroupingKeys
		%w(CTRL ARITH DATA)
	end


	def self.groupingMapManual
		map = {}
		self.manualGroupingKeys.each do |c|
			map[c] = c
		end
		map
	end

  def self.coarseGroupingMapLLVM
		map = {}
		defMap = self.defaultGroupingMapLLVM
		map["CTRL"] = defMap["CTRL_PLAIN"] +
									defMap["CTRL_ADV"]
		map["ARITH"] = defMap["INT_SUM"] +
										defMap["INT_PROD"] +
										defMap["INT_CMP"] +
										defMap["FLOAT_SUM"] +
										defMap["FLOAT_PROD"] +
										defMap["FLOAT_CMP"] +
										defMap["LOGIC"] +
										defMap["CONV"] +
										defMap["CAST"] +
										defMap["SHIFT"] +
										defMap["MISC_OP"]
		map["DATA"] = defMap["LOAD"] +
										defMap["STORE"] +
										defMap["MOVE"]
		map["OTHER"] = defMap["OTHER"]
		map
	end

	def self.defaultGroupingMapLLVM
		map = {}
		map["CTRL_PLAIN"] = %w(br switch indirectbr resume)
		map["CTRL_ADV"] = %w(invoke call cleanupret catchret ret)
		map["INT_SUM"] = %w(add sub)
		map["INT_PROD"] = %w(mul udiv sdiv urem srem)
		map["INT_CMP"] = %w(icmp)
		map["FLOAT_SUM"] = %w(fadd fsub)
		map["FLOAT_PROD"] = %w(fmul fdiv frem)
		map["FLOAT_CMP"] = %w(fcmp)
		map["LOGIC"] = %w(and or xor select)
		map["CONV"] = %w(sext sitofp trunc zext uitofp inttoptr fptoui fptosi fptrunc fpext)
		map["CAST"] = %w(ptrtoint addrspacecast bitcast)
		map["SHIFT"] = %w(shl ashr lshr)
		map["MISC_OP"] = %w(getelementptr atomicrmw cmpxchg)
		map["LOAD"] = %w(load extractelement extractvalue va_arg)
		map["STORE"] = %w(store insertelement insertvalue)
		map["MOVE"] = %w(shufflevector alloca)
		map["OTHER"] = %w(cleanuppad catchpad phi landingpad fence)
		map
	end


  def self.instructionGroupingMapLLVM
		map = {}
		self.LLVMOpcodeDictionary.values.each do |v|
			unless (v == '<Invalid operator>' || v == 'unreachable')
				map[v] = [v]
			end
		end
		map
	end

	def self.groupingForMeasureType(mt, level=2)
		if mt.include?("manual")
			grp = Grouping.new(self.groupingMapManual)
		elsif mt.include?("llvm-ir")
			if level == 1
				grp = Grouping.new(self.coarseGroupingMapLLVM)
			elsif level == 2
				grp = Grouping.new(self.defaultGroupingMapLLVM)
			else
				#individual instructions
				grp = Grouping.new(self.instructionGroupingMapLLVM)
			end
		end
		grp
	end
end