#!/usr/bin/env ruby2.0

#creates .out files containing instruction tallies
#for large sets of programs and produces a JSON
#file with the corresponding data

require_relative 'file_ops'
require_relative 'measure'
require_relative 'utils'

SPS = %w(micro combo gzip)
DTS = %w(llvm-ir intel)

MBS = %w(bsearch bbsort fib mmul qs)
TARGETS = %w(minv hash text)
OTHER = %w(shakespeare.tar)

DIFFSCRIPT = File.expand_path('difference_calc.rb', 
															File.dirname(__FILE__))
INSERTSCRIPT = File.expand_path('insert_measure.rb', 
																File.dirname(__FILE__))
																
PIN_RESULT = 'insmix.out'
PIN_EXTRA  = 'bblcnt.out'

#executes command in the shell and puts data
#from instruction tally to outfile
#input: command: shell command to run
#				data_type: intel or llvm-ir
#				outfile: destination of data
#returns: true if data type was appropriate, false otherwise
def run_metric(command, data_type, outfile)
	case data_type
		when 'intel'
			%x(#{command})
			%x(cat #{PIN_RESULT} >> #{outfile})
			File.delete(PIN_RESULT)
			File.delete(PIN_EXTRA)
			
		when 'llvm-ir'
			%x(#{command} >> #{outfile})
		else false
	end
	true
end

#produces a single .out file containg instruction tally for program
#input: name: name of .out file
#       prog: base name for program (without extension)
#       dt:   type of data (intel or llvm ir) we want
#       args: arguments to program
#       pl:   path to pintools insmix.so library
#       diff: whether we get data for the bulk program
#             or difference between running workload twice
#             or once
#returns: path to .out file or nil in case of failure

def generate_single_out_file(name, prog, dt, args, pl=nil, diff=true)

  case dt
    when 'intel'
      return nil unless pl
      base_com = "pin -t #{pl} -- ./#{prog}.exe "
      file_tag = 'pin'
    when 'llvm-ir'
      base_com = "./#{prog}.ir "
      file_tag = 'ir'
    else
      return nil
  end

  args.each do |a|
    base_com += "#{a} "
  end

  outfile = File.new("#{name}.#{file_tag}.out", 'w')
  outfile.write("#{name}\n")
  
  outfile.write("#{dt}")
  outfile.write(diff ? "\n" : "-raw\n")
  outfile.close

  if diff
    of1 = File.new("#{File.basename(outfile.path)}.1", 'w')
    of1.write("#{name}\n")
    of1.write("#{dt}-raw\n")
    of1.close
    run_metric("#{base_com} 1 ",dt,of1.path)
    

    of2 = File.new("#{File.basename(outfile.path)}.2", 'w')
    of2.write("#{name}\n")
    of2.write("#{dt}-raw\n")
    of2.close
    run_metric("#{base_com} 2 ",dt,of1.path)
    #%x(#{base_com} 2 >> #{of2.path})

    %x(#{DIFFSCRIPT} #{of1.path} #{of2.path} >> #{outfile.path})

    File.delete(of1.path)
    File.delete(of2.path)

  else
  	run_metric(base_com,dt,outfile.path)
    #%x(#{base_com} >> #{outfile.path})
  end

  outfile.path
end

#gets possible arguments for executables
#input: executable base name
#return: all valid arguments
#       for micro, names of all build-in test programs
#       for combo, all combinations of 3 microbenchmarks
#       for gzip, the name of test file to compress (shakespeare.tar)

def arg_list_for_prog(prog)
  case prog
    when 'micro'
      return MBS + TARGETS
    when 'combo'
      return MBS.combination(3)
    when 'gzip'
    	File
      return OTHER
    else
      nil
  end
end

#makes tag for the data to be used as key in json file
#input: prog: executable base name
#       args: argument(s) passed to it
#returns: key to be used in json or nil if invalid data
#for gzip, data is keyed under 'gzip'
#for the individual test programs, run by micro, we use
#the name of the program
#for combination runs we use the names of programs separated
#by underscores, i.e. qs_fib_bsearch

def data_tag(prog, args)
  case prog
    when 'micro'
      return args
    when 'combo'
      return "#{args[0]}_#{args[1]}_#{args[2]}"
    when 'gzip'
      return prog
    else
      nil
  end
end

#---------script body----------------------------------------

unless ARGV.length >= 2
  puts 'Invalid argument number'
  exit
end



dest_file   = ARGV[0] #json file for output
source_prog = ARGV[1] #executable to run, can be "all"
data_type   = ARGV[2] ? ARGV[2] : "llvm-ir" #can be "all"

pin_library = ARGV[3] ? ARGV[3] : nil

sps = source_prog == 'all' ? SPS : [source_prog]
dts = data_type == 'all' ? DTS : [data_type]

sps.each do |p|
  al = arg_list_for_prog(p)
  dts.each do |d|
    al.each do |a|
      pn = data_tag(p,a)

      if p == 'gzip' #make a copy of target file so that we can reuse it
      							 #later
      	get_diff = false
      	ta = "_temp#{a}"
      	%x(cp #{a} #{ta})
      	%x(rm -f *.tar.gz) #will prompt if destination already exists
      	arg = [ta]
      else
      	get_diff = true
      	arg = a.instance_of?(Array) ? a : [a]
      end
			
      of = generate_single_out_file(pn, p, d,
                                    arg, pin_library, get_diff)
      #puts "#{INSERTSCRIPT} #{of} #{dest_file}"
      %x(#{INSERTSCRIPT} #{of} #{dest_file})
      File.delete(of)
    end
  end
end

