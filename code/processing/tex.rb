require_relative 'float_extra'

class Tex

  def self.long_table_header(colNumber)
    res = "\\begin{center}\n"
    res +=  "\\begin{longtable}{|"
    colNumber.times do
      res += " c |"
    end
    res += "}\n"
    res += "\\hline\n"
    res
    end

  def self.long_table_footer
    res = "\\caption{Table Title}\n"
    res += "\\label{tab:lab}\n"
    res += "\\end{longtable}\n"
    res += "\\end{center}\n"
    res
  end

  def self.texTableHeader(colNumber)
    res =  "\\begin{table}[ht]\n"
    res += "\\begin{center}\n"
    res += "\\begin{tabular}{|"
    colNumber.times do
      res += " c |"
    end
    res += "}\n"
    res += "\\hline\n"
    res
  end

  def self.texTableLine(elems, nomod=false)
    res = ""
    elems.each_with_index do |e, i|
      if e.is_a?(Float)
        res += e.to_4s
      elsif e.is_a?(Integer)
        res += e.to_s
      else
        if nomod
          res += e
        else
          res += e.to_tex
        end
      end


      if i != (elems.length-1)
        res += " & "
      end
    end

    res += " \\\\ "

    res
  end

  def self.textLineSeparator
    "\n\\hline\n"
  end

  def self.texTableFooter
    res =  "\\end{tabular}\n"
    res += "\\end{center}\n"
    res += "\\caption{Table Title}\n"
    res += "\\label{tab:lab}\n"
    res += "\\end{table}\n"
    res
  end

end