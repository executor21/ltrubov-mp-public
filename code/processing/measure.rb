require_relative 'grouping'

class Measure
  attr_reader :type
  attr_reader :vals

  def initialize(vals: {}, type: "unknown")
    @vals = vals
    @type = type
  end

	def grouped_data(grouping)
		res = {}
		@vals.each do |k,v|
			ocg = grouping.groupForOpcode(k)
			if ocg
				if res[ocg]
					res[ocg] += v
				else
					res[ocg] = v
				end
			end
		end
		res
	end

end
