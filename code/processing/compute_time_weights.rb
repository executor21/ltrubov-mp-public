#!/usr/local/bin/ruby2.0

require_relative 'utils'
require_relative 'tex'
require_relative 'optimization'
require_relative 'data_storage'

$alt_data = false
$decomps = []

#computes a virtual time of a linear
#addtion of benchmarks in mbs (mbs = [1,2,3] will use
# b1, b2, and b3), on system system (system = 1 will use
# C1) weighted by weights in w
def virtual_time(system, mbs, w)
  t = $alt_data ? MICRO_TIMES_ALT : MICRO_TIMES
  return w[0]*t[mbs[0]][system] + w[1]*t[mbs[1]][system] + w[2]*t[mbs[2]][system]
end

def virtual_ratio(systems,mbs,w)
  t = $alt_data ? MICRO_TIMES_ALT : MICRO_TIMES
  r = [t[mbs[0]][systems[0]]/t[mbs[0]][systems[1]],
       t[mbs[1]][systems[0]]/t[mbs[1]][systems[1]],
       t[mbs[2]][systems[0]]/t[mbs[2]][systems[1]]]

  (r[0]**w[0])*(r[1]**w[1])*(r[2]**w[2])

end

def error(target, mbs, sys, w)
  _T = $alt_data ? MACRO_TIMES_ALT : MACRO_TIMES

  real_ratio = _T[target][sys[0]]/_T[target][sys[1]]
  pred_ratio = virtual_time(sys[0], mbs, w)/virtual_time(sys[1],mbs,w)

  (real_ratio - pred_ratio).abs/real_ratio
end

def find_decomp(tar, mbs, sys)
  $decomps.each do |d|
    if d.target == tar && d.micros == mbs && d.systems == sys
      return d
    end
  end
  nil
end

#---------script body----------------------------------------

unless ARGV.length >= 2
  puts 'Invalid argument number'
  exit
end

from_file = false

if File.exist?(ARGV[0])
  from_file = true
  $decomps = Marshal.load(File.read(ARGV[0]))
elsif ARGV[0] == "alt"
  $alt_data = true
end

use_nm = ARGV[1] ? ARGV[1] == "nelder_mead" : false
use_arith_mean = ARGV[2] ? ARGV[2] == "arithmetic" : false

micros = [1,2,3,4,5]
micro_combos = micros.combination(3)

targets = [1,2,3]

systems = [1,2,3,4]
sys_combos = systems.combination(2)

unless from_file
  wr = Utils.weight_range(0.001)
  #puts wr.length

  targets.each do |tar|
    micro_combos.each do |c|
      sys_combos.each do |s|
        d = Decomp.new(tar, c, s)
        err = Float::MAX
        w_best = [0.0,0.0,0.0]

        err_func = nil

        if use_arith_mean
          #brute-force minimization
          err_func = Proc.new do |w|
            error(tar,c,s,w)
          end
        else
          _T = $alt_data ? DataStorage:: MACRO_TIMES_ALT :
                  DataStorage::MACRO_TIMES
          real_ratio = _T[tar][s[0]]/_T[tar][s[1]]
          t = $alt_data ? DataStorage::MICRO_TIMES_ALT :
                DataStorage::MICRO_TIMES
          r = [t[c[0]][s[0]]/t[c[0]][s[1]],
               t[c[1]][s[0]]/t[c[1]][s[1]],
               t[c[2]][s[0]]/t[c[2]][s[1]]]

          err_func = Proc.new do |w|
            pred_ratio = (r[0]**w[0])*(r[1]**w[1])*(r[2]**w[2])
            (real_ratio - pred_ratio).abs/real_ratio
          end



        end

        if use_nm
          w_best = Optimization.nelder_mead(wr,err_func)
          err = err_func.call(w_best)
        else
          wr.each do |w|
            e_curr = err_func.call(w)
            if e_curr < err
              w_best = w
              err = e_curr
            end
          end
        end



        d[:weights] = w_best
        d[:errorX] = err
        $decomps.push(d)
      end
    end
  end

  data_file = ARGV[0] ? ARGV[0] :
            $alt_data ? 'alt.decomp.rbobj' :
                'main.decomp.rbobj'

  File.open(data_file, 'w') {|f| f.write(Marshal.dump($decomps)) }
end


#exit

out_targ = targets

sca = sys_combos.to_a

out_targ.each do |tar|
  out = Tex.long_table_header(7)
  sca.each_slice(3).each do |scasl|
    titles = ["\\textbf{\\shortstack{Decomposition\\\\Components}}"]
    scasl.each do |sc|
      titles.push("${\\mathbf{w}(#{sc[0]},#{sc[1]})}$")
      titles.push("\\shortstack{${E_{\\mathcal{X}}(B_#{tar},\
                    C_#{sc[0]},}$\\\\${C_#{sc[1]})}$}")
    end
    out += Tex.texTableLine(titles, true)
    out += Tex.textLineSeparator
    micro_combos.each do |mc|
      line = ["${b_#{mc[0]},b_#{mc[1]},b_#{mc[2]}}$"]
      scasl.each do |sc|
        d_rel = find_decomp(tar,mc,sc)
        if d_rel
          w = d_rel[:weights]
          line.push("\\shortstack{#{w[0].to_4s},\\\\#{w[1].to_4s},\\\\#{w[2].to_4s}}")
          line.push(d_rel[:errorX])
        end
      end
      out += Tex.texTableLine(line, true)
      out += Tex.textLineSeparator
    end

    out += "\n\\\\\n"
  end



  out += Tex.long_table_footer
  puts out
  puts ''
end