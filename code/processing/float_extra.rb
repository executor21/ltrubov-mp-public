class Float
  def to_4s
    sprintf('%.4g', self)
  end

end

class String
  def to_tex
    if self.include?("_")
      return self.sub("_", "\\_")
    end

    self
  end
end