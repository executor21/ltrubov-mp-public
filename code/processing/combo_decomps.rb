#!/usr/local/bin/ruby2.0

#produces a tex table listing the weights for the best
#decompositions of programs explicitly generated from
#running decomposition component programs in sequence
#such decompositions should be guaranteed to have small
#errors
#weights generated should be the same as those used in the
#original combo. If all 3 components were run once, the
#composite program should decompose with weights of 1/3

require_relative 'benchmark'
require_relative 'measure'
require_relative 'file_ops'
require_relative 'grouping'
require_relative 'utils'
require_relative 'data_storage'

$decomps = []
$must_recompute = true

#find an existing decomposition in $decomps array
#input: tar: the decomposition target
#       mbs: the decomposition components
#returns: decomposition structure or nil if not found
def find_decomp(tar, mbs)
  $decomps.each do |d|
    if d[:target] == tar && d[:micros] == mbs
      return d
    end
  end
  nil
end

#---------script body----------------------------------------

unless ARGV.length >= 3
  puts 'Invalid argument number'
  exit
end

decomp_data_file = ARGV[0] #marshalled object
combo_json_file = ARGV[1]  #json file with instruction
                           #frequencies of combo programs
micro_json_file = ARGV[2]  #json file with instruction frequencies
                           #of decompositon components

micros = %w(bsearch bbsort fib mmul qs)
micro_combos = micros.combination(3)

grouping_level = 2 #default

if  File.exist?(decomp_data_file)
  $decomps = Marshal.load(File.read(decomp_data_file))
  if $decomps.length > 0
    $must_recompute = false
  end
end

if $must_recompute

  combo_progs = FileOps.load_benchmark_programs(combo_json_file)
  micro_progs = FileOps.load_benchmark_programs(micro_json_file)

  met='llvm-ir'
  gr = Grouping.groupingForMeasureType(met,grouping_level)

  gd = {}
  (micros).each do |t|
    p = Benchmark.benchmark_named(micro_progs, t)
    gd[t] = Utils.normalize_hash(p.grouped_data(gr, met))
  end

  combo_progs.each do |cp|
    gd[cp.name] = Utils.normalize_hash(cp.grouped_data(gr, met))
  end


  wr = Utils.weight_range(0.001)

  micro_combos.each do |mc|
    combo_name = "#{mc[0]}_#{mc[1]}_#{mc[2]}"
    puts "#{combo_name}:"
    d = DataStorage::Decomp.new(combo_name,mc)
    curr_progs = [
        Benchmark.benchmark_named(micro_progs, mc[0]),
        Benchmark.benchmark_named(micro_progs, mc[1]),
        Benchmark.benchmark_named(micro_progs, mc[2])
    ]
    err = Float::MAX
    w_best = [0.0,0.0,0.0]
    wr.each do |w|
      cp = Benchmark.combo_prog(curr_progs,w,met,grouping_level)
      e_curr = Utils.percent_distance(cp, gd[combo_name])
      if e_curr < err
        w_best = w
        err = e_curr
      end
    end
    d[:weights] = w_best
    d[:error] = err
    $decomps.push(d)

  end

  File.open(decomp_data_file, 'w') {|f| f.write(Marshal.dump($decomps)) }
end

micro_combos.each do |mc|
  combo_name = "#{mc[0]}_#{mc[1]}_#{mc[2]}"
  dec = find_decomp(combo_name,mc)
  puts "#{mc}: #{dec[:weights]} #{dec[:error]}"
end


out = Tex.texTableHeader(3)
titles = [
    "\\textbf{\\shortstack{Program\\\\Components}}",
    "\\textbf{\\shortstack{Decomposition\\\\weights}}",
    "\\textbf{Distance}"
]
out += Tex.texTableLine(titles, true)
out += Tex.textLineSeparator


micro_combos.each do |mc|
  combo_name = "#{mc[0]}_#{mc[1]}_#{mc[2]}"

  dec = find_decomp(combo_name,mc)
  if dec
    line = ["#{mc[0]},#{mc[1]},#{mc[2]}"]
    w = dec[:weights]
    line.push("#{w[0].to_4s},#{w[1].to_4s},#{w[2].to_4s}")
    line.push(dec[:error])
    out += Tex.texTableLine(line, true)
    out += Tex.textLineSeparator
  end
end

out += Tex.texTableFooter
puts out
