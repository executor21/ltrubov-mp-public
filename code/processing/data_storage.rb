#This class contains results of timing measurements of test programs
#It uses the output of compute_average_times.rb, manually pasted into
#this file

class DataStorage

  #These are timing measures for the five microbenchmarks used as
  #decomposition components
  #the array is 1-based indexed, so that MICRO_TIMES[i][j] is the
  #timing measure of b_i on C_j, as referenced in the project report
  MICRO_TIMES = [
      [nil], #dummy for 1-based indexing
      ["bsearch:", 4.833499999999802e-08, 1.8999999999999545e-07, \
            4.7507999999995226e-08, 3.633899999999457e-08],
      ["qs:", 0.20573626000000003, 0.4739, 0.19224221000000002, 0.1550131],
      ["mmul:", 0.01824489999999996, 0.06629999999999868, 0.020264859999999985, \
          0.015195509999999982],
      ["bbsort:", 0.18880275, 0.566, 0.1897749, 0.15207088],
      ["fib:", 0.08790553999999995, 0.18989999999999924, 0.09196259999999995, \
          0.06353552999999994]
  ]

  #These are timing measures for the three benchmarks used as
  #decomposition targets
  #the array is 1-based indexed, so that MACRO_TIMES[i][j] is the
  #timing measure of B_i on C_j, as referenced in the project report
  MACRO_TIMES = [
      [nil], #dummy for 1-based indexing
      ["minv:", 0.03724071999999997, 9.841199999999999, 0.04083438999999996, \
        0.055475820000000044],
      ["hash:", 0.0022587000000000934, 0.005300000000000966, 0.0021354000000000186, \
        0.0018219899999999535],
      ["text:", 0.005211209999999973, 0.009499999999999705, 0.004965450000000047, \
        0.004663409999999963]
  ]

  #These are timing measures for the five microbenchmarks used as
  #decomposition components on the systems
  #the array is 1-based indexed, so that MICRO_TIMES[i][j] is the
  #timing measure of b_i on C_j, as referenced in the project report
  MICRO_TIMES_ALT = [
      [nil], #dummy for 1-based indexing
      ["bsearch:", 4.833499999999802e-08, 4.7507999999995226e-08, 4.123030000000512e-08, \
        3.848200000000368e-08],
      ["qs:", 0.20573626000000003, 0.19224221000000002, 0.143286352, 0.14687232],
      ["mmul:", 0.01824489999999996, 0.020264859999999985, 0.013867423999999972, \
        0.013920260000000031],
      ["bbsort:", 0.18880275, 0.1897749, 0.12336598700000001, 0.12954151999999997],
      ["fib:", 0.08790553999999995, 0.09196259999999995, 0.06098461399999996, \
        0.06296403999999996]
  ]

  MACRO_TIMES_ALT = [
      nil, #dummy for 1-based indexing
      ["minv:", 0.03724071999999997, 0.04083438999999996, 0.040014411, 0.04115494999999994],
      ["zip:", 0.4304287458799999, 0.51025916585, 0.00025611, 0.36213523536],
      ["compile:", 0.8351679246999999, 0.71340815135, 0.00026670999999999997, 0.36047282318]
  ]

  #This structure is used to store intermediate results when computing
  #the best decompositions, and to Marshal the results to a ruby object
  #file so that recomputation can be avoided.
  Decomp = Struct.new(:target, :micros, :systems, :weights, :error)

end