#!/usr/bin/env ruby2.0

require_relative 'benchmark'
require_relative 'measure'
require_relative 'file_ops'
require_relative 'grouping'
require 'json'

#functions




#---------script body----------------------------------------

dataFile = ARGV[0]

targets = %w(bbsort bsearch fib mmul qs minv hash text)

progs = FileOps.load_benchmark_programs(dataFile)

met="llvm-ir"
gr = Grouping.groupingForMeasureType(met)

gd = []
targets.each do |t|
  p = Benchmark.benchmark_named(progs, t)
  gd.push(Utils.normalize_hash(p.grouped_data(gr, met)))
end

out = Tex.texTableHeader(targets.length+1)
out += Tex.texTableLine(["Instruction Type"] + targets)
out += Tex.textLineSeparator

gr.groupMap.keys.each do |k|
  line = [k]

  targets.length.times do |i|
    val = gd[i][k] || 0.0
    line.push(val)
  end

  out += Tex.texTableLine(line)
  out += Tex.textLineSeparator
end

out += Tex.texTableFooter

puts out
puts ''

#----------COMBO DATA------------------------------

unless ARGV[1]
  exit
end

micros = %w(bsearch bbsort fib mmul qs)
micro_combos = micros.combination(3)
combo_progs  = FileOps.load_benchmark_programs(ARGV[1])

gd = []
mc_table_tiles = []
micro_combos.each do |mc|
  pname = "#{mc[0]}_#{mc[1]}_#{mc[2]}"
  p = Benchmark.benchmark_named(combo_progs, pname)
  gd.push(Utils.normalize_hash(p.grouped_data(gr, met)))
  mc_table_tiles.push(
      "\\textbf{\\shortstack{#{mc[0]}+\\\\#{mc[1]}+\\\\#{mc[2]}}}")
end



out = Tex.texTableHeader(mc_table_tiles.length+1)
out += Tex.texTableLine(["Instruction Type"] + mc_table_tiles)
out += Tex.textLineSeparator

gr.groupMap.keys.each do |k|
  line = [k]

  mc_table_tiles.length.times do |i|
    val = gd[i][k] || 0.0
    line.push(val)
  end

  out += Tex.texTableLine(line)
  out += Tex.textLineSeparator
end

out += Tex.texTableFooter

puts out