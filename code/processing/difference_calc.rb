#!/usr/bin/env ruby2.0

require_relative 'file_ops'
require_relative 'measure'
require_relative 'utils'

#---------script body----------------------------------------

unless ARGV.length == 2
  puts 'Invalid argument number'
  exit
end

il1 = FileOps.lines_of_file(ARGV[0])
m1,d1 = FileOps.process_data_file(il1)

il2 = FileOps.lines_of_file(ARGV[1])
_,d2 = FileOps.process_data_file(il2)

diff = Utils.abs_hash_difference(d1, d2)

dlms = FileOps.delimiters_for_measure_type(m1)

puts dlms[0]
Utils.neat_print_hash(diff)
puts dlms[1]

