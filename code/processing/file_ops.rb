#This class is the interface between text files produced by programs
# of this project as output and either Ruby objects for other programs
# to work with, or common data storage formats such as JSON

require 'json'
require_relative 'benchmark'
require_relative 'measure'

class FileOps

	#input: path to JSON file storing the instruction breakdown
	#data for selected programs
	#returns: array of Benchmark objects containing the same data
  def self.load_benchmark_programs(fn)
    res = []
    if File.exists?(fn)
      data = JSON.parse(File.new(fn).read)
      data.each do |k,v|
        np = Benchmark.new(name: k)
        v.each do |kk,vv|
          nm = Measure.new(vals: vv, type: kk)
          np.measures.push(nm)
        end
        res.push(np)
      end
    end
    res
  end

	#input: programs: an array of Benchmark objects
	#				tf: path to JSON file where the data will be stored
	#saves the data in programs to tf
  def self.save_benchmark_programs(programs, tf)
    if File.exists?(tf)
      File.delete(tf)
    end

    data = {}
    programs.each do |p|
      data[p.name] = p.data
    end

    json_file = File.new(tf, 'w')

		json_file.write(JSON.pretty_generate(data,
                                 indent: '  ',
                                 object_nl: "\n"))
		json_file.close

  end

	#input: path to file
	#output: array containing lines of the file as strings, stripped
	#of leading and trailing whitespace
  def self.lines_of_file(file_path)
    f = File.open(file_path)
    lines = f.readlines
    f.close

    lines.map {|l| l.strip}
  end

	#input: a type of breakdown measure being studied
	#output an array of two elements, with the first
	#containg the element that, for that measure preceeds
	#data about the actual breakdown, and the second the string
	#that follows such data, bounding the parts of file this program
	#is interested in
  def self.delimiters_for_measure_type(mt)
		dlm = nil
		if mt.include?('manual')
			dlm = %w(Statement TOTAL)
		elsif mt.include?('llvm')
			dlm = %w(BEGIN_INSTRUCTION_BREAKDOWN END_INSTRUCTION_BREAKDOWN)
		elsif mt.include?('intel')
			dlm = %w($dynamic-counts *)
		end

		dlm
	end

	#isolates the instruction opcode and count
	#in a line in the text file output by a metrics program
	#input: line: string containg text of line from output file
	#				mt: type of measure studied
	#returns: opcode (string) and count (number) corresponding to
	#the line
	def self.key_and_value_for_line_data(line, mt)
		key,val = nil,nil
		if mt.include?('intel-raw')
			line_comps = line.split(' ')
			unless line_comps[0].include?('#')
				key = line_comps[1]
				val = line_comps[2].to_i
			end
		else
			line_comps = line.split(':')
			key = line_comps[0].strip
			val = line_comps[1].to_i
			if mt.include?('llvm-ir-raw')
				key = Grouping.LLVMOpcodeDictionary[key]
			end
		end

		return key,val
	end

	#creates a hash of instruction opcodes and counts corresponding to
	#data in file
	#input: fl is array of strings representing lines of file
	#				fl[1] should contain the measure type
	#returns: a hash with opcodes as keys and corresponding counts as values
	def self.process_data_file(fl)
		measure_type = fl[1]

		delimiters = delimiters_for_measure_type(measure_type)
		in_data = false
		data = {}
		fl.slice(2,fl.length).each do |line|
			unless in_data
				if line.include?(delimiters[0])
					in_data = true
				end
				next
			end
			
			if line.include?(delimiters[1])
				break
			end
			

			dk,dv = key_and_value_for_line_data(line, measure_type)
			if dk && dv
				data[dk] = dv
			end
		end

		return measure_type,data
	end
end
