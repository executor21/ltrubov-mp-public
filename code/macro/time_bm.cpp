#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <chrono>
#include <ctime>
#include <ratio>

using namespace std::chrono;


#define TIMING_ITERATIONS 10

int main(int argc, char *argv[]){


	const char* bmNames[2] = {"compile", "zip"};
	const char* commands[2] = {
		"g++ sample.cc -o sample.exe",
		"cp shakespeare.tar shakespeare1.tar && ./gzip -f shakespeare1.tar"
	};

	const char* clean = "";
	int res;

	double speeds[2] = {0.0, 0.0};

	high_resolution_clock::time_point t1, t2, t3;
	duration<double> span12, span23;//, span;

	for(int i = 0; i < 2; i++){
		t1 =  high_resolution_clock::now();
		for(int j = 0; j < TIMING_ITERATIONS; j++){
			res = system(commands[i]);
		}
		t2 = high_resolution_clock::now();
		for(int j = 0; j < TIMING_ITERATIONS; j++){
			res = system(commands[i]);
			res = system(commands[i]);
		}
		t3 = high_resolution_clock::now();



		span12 = duration_cast<duration<double> >(t2 - t1);
		span23 = duration_cast<duration<double> >(t3 - t2);

		//duration<double> span = duration_cast<duration<double>>(span23 - span12);

		speeds[i] = (span23.count() - span12.count())/TIMING_ITERATIONS;

		//printf("%s %f %f %f\n", bmNames[i], t1, t2, t3);

	}

	for(int i = 0; i < 2; i++){
		if(speeds[i] != 0.0){
			printf("%s: %.17g seconds\n", bmNames[i], speeds[i]);
		}
	}
}
