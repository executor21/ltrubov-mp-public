#!/usr/bin/env ruby

require 'date'
require 'net/http'

def toRoman(num)
	res = ""
	if num >= 100
		res += "C"
		res += toRoman(num-100)
	elsif num >= 90
		res += "XC"
		res += toRoman(num-90)
	elsif num >= 50
		res += "L"
		res += toRoman(num-50)
	elsif num >= 40
		res += "XL"
		res += toRoman(num-40)
	elsif num >= 10
		res += "X"
		res += toRoman(num-10)
	elsif num >= 9
		res += "IX"
		res += toRoman(num-9)
	elsif num >= 5
		res += "V"
		res += toRoman(num-5)
	elsif num >= 4
		res += "IV"
		res += toRoman(num-4)
	elsif num >= 1
		res += "I"
		res += toRoman(num-1)
	end

	res
end

def targetSite
	return "http://shakespeare.mit.edu/"
end

def sonnetSubsite
	return "Poetry/"
end


def processPlays(links)
	res = {}
	links.each do |l|
		name = l.sub("/index.html", "")
		fullLink = "#{targetSite}#{name}/full.html"
		res[name] = fullLink
	end
	res
end

def processSonnets(link)
	res = {}
	(1..154).each do |sn|
		name = "sonnet.#{toRoman(sn)}"
		fullLink = "#{targetSite}#{name}.html"
		res[name] = fullLink
	end
	res
end

def processPoems(links)
	res = {}
	links.each do |l|
		name = l.sub(sonnetSubsite, "")
		fullLink = "#{targetSite}#{l}"
		res[name] = fullLink
	end
	res
end

#home page, listing all works

if ARGV.length > 0
	destDir = "#{ARGV[0]}/"
else
	destDir = ""
end

uri = URI(targetSite)
response = Net::HTTP.get(uri)

matches = response.scan(/[A-z0-9_]*\/[A-z0-9_]*.html/)
matches -= ["TR/REC-html"]

sonnets = matches.length - 5

fullLinks = {}
fullLinks = fullLinks.merge(processPlays(matches.slice(0...sonnets)))
fullLinks = fullLinks.merge(processSonnets(matches[sonnets]))
fullLinks = fullLinks.merge(processPoems(matches.slice((sonnets+1)..-1)))

fullLinks.each do |k, fl|
	uri = URI(fl)
	response = Net::HTTP.get(uri)
	f = File.new("#{destDir}#{k}.html", 'w')
	f.write(response)
	f.close
end
