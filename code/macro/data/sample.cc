#include <iostream>
using namespace std;

#include<time.h>

int IFcnt[285],IFEcnt[285],SWcnt[285],WHILEcnt[285],DOcnt[285],FORcnt[285];

int F1(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   do
   {
      l -= (b+e-k-i-f-n+a+i+g)%100;
      a += (g*b-e+b-e+l*f-b)%100;
      l -= (l-n*m+a-n)%100;
      g += (d*c-d-i-d+b*m+m-m)%100;
      l += (g*f+l+i*k*f+g*i+a-l*m*l-d*d)%100;
   } while( ++DOcnt[0]%5 );
   e -= (h-d+k-g-i*d*d-a*n+k)%100;
   n += (k*k-f)%100;
   m -= (h*f*b-j*l+i)%100;
   k += (d+e+j-k-e-l+n+i+n-g+h-i)%100;
   h += (h-c-l*h+m*i+b)%100;
   j -= (d-a+d-c+n+d)%100;
   a += (k-h*h-g-n+m*l-k*l-a+a-b+e)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F2(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l += (j+f-i+i-e-d-c-i+n*g*j-i*f)%100;
   c += (d-c-k*i*b+g-f*f-i+f*g-a-k*m)%100;
   a -= (e+f)%100;
   a -= (m*i+h*i-l+h-n+i*l)%100;
   d -= (d+c-e+m-i+g)%100;
   a += (d+j+k-j)%100;
   e -= (g-j+m*g-n)%100;
   j += (b+e+e-j+e*m+a-e)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F3(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l  = (i+c*l+i-m+e)%100;
   d -= (k-g*h+n+f-a+k+k+g-a*d)%100;
   j -= (h*n*n-b-a+b-e*e+d+n-g+n)%100;
   h += (g*e+h+n*a*i*i-f+d+n*i+f)%100;
   k -= (g*k-b-d-d+n*j-m+n+i)%100;
   f += (b-f*e*k-l)%100;
   d += (h-h-l)%100;
   d += (m*i+i+i*m+i+k+n-a+e+i-d-b)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F4(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f -= (m*h-h-a-h+c-i-a+b-j)%100;
   j += (b*l-b*a+b+g*b+n+k+c+c-a)%100;
   j  = (i+f+j+l-h*n+c-a-e+h)%100;
   d += (a+k-k*a-e*g-d+n+b-i-m)%100;
   e -= (f-m+l-n+i*h-d-h-d+n)%100;
   f -= (j-b)%100;
   b += (i+n*c-l*i+d*e)%100;
   b -= (h+i*h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F5(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l -= (d+h)%100;
   j += (k+e-c+m)%100;
   g -= (n-l+l-c-m*b*n*m+e-k-b-j*e)%100;
   i += (l+j*i-f*f)%100;
   e += (c*e*f+a+f+j)%100;
   n  = (m+a-i+b+a-h)%100;
   b += (e-g-n+e+k-n)%100;
   f -= (f-h*g*j*l+b)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F6(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   e += (k-k-m*f)%100;
   h  = (j-b+k-j*e+a-f*n)%100;
   a += (a-e+n-a*b+a-k+j-e*d*c+c)%100;
   c += (g*n-e-n-d-e-f*n)%100;
   n -= (m-e)%100;
   n += (a+l-c-l+l-d+l-e-k-e*g)%100;
   e -= (l-l-e)%100;
   n += (c+j+m)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F7(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   j += (m+c+b+m+a)%100;
   j += (m+j+h-i+j+a)%100;
   m -= (f-m-l+a)%100;
   f += (c-k-n-b*f*a-b-a-c+b-g+f*b*h)%100;
   d += (k+c)%100;
   h -= (j-m*j+m-d-g-c-g+m-h)%100;
   b -= (d-e*b)%100;
   e -= (a-d-i+c-d*b-a-m)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F8(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   n += (m*i-m-b)%100;
   a += (a+i-n-l-a-a-i)%100;
   d -= (b+h)%100;
   d += (n+i+g+i-k)%100;
   g -= (j-a-d+i-a+l)%100;
   d += (d*f+l+i*j+d+c-e-c+h-j+i+h)%100;
   f -= (k-b+c+c+f*h-j-e+e-c)%100;
   a += (l-n*i+m-g-k+b+a)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F9(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   e  = (m*m-l)%100;
   j -= (i-c-d*h*h+m-g-n+a+g+n+h-n+i)%100;
   l += (c+k)%100;
   j += (a*n)%100;
   i -= (i-g+f-i*l+j*d+a-e)%100;
   i += (i*g-b+c*h-l+b+i+m+c-j-f+c)%100;
   l += (f+g+i*g)%100;
   m += (j*f-c-i-j)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F10(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   m -= (d*f*d*j-b-h-c+e+h-i+m)%100;
   h -= (g*e-c+c-l-b-a+g)%100;
   m += (h+b+g+n-i-g+i-i-h-d*b-e)%100;
   g += (d+f*f+k+m-a+e-c-i+g-k+l)%100;
   g += (l+n+c-l+c-h+g)%100;
   j += (e*i*k+g-a-e-j-d-m+h+d-k-k-a)%100;
   l -= (d-b-l+b+f-k-a+j+m-a-a)%100;
   g -= (m+i-d+d*d-d+h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F11(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l += (n+m-h*e-j)%100;
   k -= (h-g+e-e-g+i+i+k)%100;
   f += (i+c*h-h+i*g-m+h+d*d-c*i*d+k)%100;
   n -= (i-d)%100;
   m += (i*h-l+i-f*a-e-j-k-a-j-h*f)%100;
   m += (l+l-j-i)%100;
   e -= (c+i*h-d-g)%100;
   m  = (j+i+k)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F12(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   i += (a-a-a-m+c-h+c)%100;
   i -= (e*g+d*n+a+c-f)%100;
   d -= (k+f+d-j)%100;
   d += (g*i-l+e+h+i-l*a*c+n)%100;
   c  = (c-j-n*c-j-d*c-a+l*k)%100;
   h += (g+c-j-n+j+f+d*l-k)%100;
   e += (e+g+e*h)%100;
   h -= (k+b-l*h+k+e-h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F13(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   h -= (b*j)%100;
   h -= (i+f+m-d-n*h*m+b-g)%100;
   k -= (k-m-a*f+b+b-i*m+a-c)%100;
   f -= (m+e+n-f+a-f+b-n-g+n-a+i)%100;
   f  = (j-d)%100;
   l -= (m+i+a-b-d-g+k+m+m)%100;
   h -= (l+c-d)%100;
   k += (e*d)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F14(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f -= (i-j*j*d-n-b*d-d-c+m)%100;
   b += (g+a+l+d+a*n-e-h)%100;
   l -= (h-e+f+c+i-d)%100;
   l += (c+i-h-b*l)%100;
   f += (j+j)%100;
   h += (f-n-e*c*i-e*c+j-m+k-h)%100;
   c += (l-k-g-l)%100;
   b += (d-j-i)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F15(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l -= (k-k+d-b-h+g-j-c*i+f+n+j)%100;
   i += (g*d*a+d+g-a+b+g)%100;
   j  = (i+g+m+g*m+g*n)%100;
   e -= (g+e)%100;
   e -= (c-k)%100;
   m -= (e*e-i+b+a-k-f+j+k)%100;
   h += (l+a+h+d+k*i-c+g*m+n-h*a+b-a)%100;
   j -= (l*b-j+n+j+m-b-e-h+m-m*g+e+f)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F16(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   i += (j-g+f)%100;
   m -= (c+h+e-l+n)%100;
   c += (n-h-j*b+n*d-j+i+n+k-b-n-m)%100;
   h -= (a*c+g-f+f*m+j*j-f-g)%100;
   g -= (a-e*e)%100;
   e += (b+m+a+l+h-h-c)%100;
   l += (c-e)%100;
   j += (h+i+f+b*e+n+c-l)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F17(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f -= (a*k-g-g*d+a-m+m-i)%100;
   c -= (c+d)%100;
   m -= (f+g*m-b-m-b)%100;
   n -= (m-e+g*a*b)%100;
   f += (g-h-e*e+k-i)%100;
   l -= (a*h)%100;
   j -= (g+m+n*a+g-g-l)%100;
   e -= (a*j*d-f*e-g)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F18(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   g += (f+e+a-h+d-b+b-j+c-c*k-e*i)%100;
   h += (b+g+a-b+b)%100;
   j -= (f-c-j-e+l-h+n+n+f+m-m+l-k+n)%100;
   d += (k-n-j-b+l)%100;
   d -= (k+n+d+l*m-h+b+g+g)%100;
   e -= (c+n+l-a+l)%100;
   h += (e-j)%100;
   a -= (d*d)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F19(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   i -= (i-e+g)%100;
   k += (e*d+f-d*c+m+b)%100;
   l += (d-g+l-m+h-m-c+b-b-h-g)%100;
   c += (b*c*b)%100;
   m += (k+b+d*c-c)%100;
   a += (c-d-j-f-e*a+e+a-f*l-n+h-c)%100;
   j += (a+h)%100;
   b += (h+j-j-h-h+a-k*l-a)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F20(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   m += (d*e-n+k-i-k+m+h*c+a-h)%100;
   h -= (n*n-j+m*g-h*a+i)%100;
   l += (h*h*f*m-l-d+n+h)%100;
   k -= (l-a+f-g-n*e*g*a*k)%100;
   j += (a-a+b*n-a-k*n+g*d)%100;
   b  = (d+a+l+g-h*j*k)%100;
   i -= (c+f+h+e-j-m-n*c-l+g*d+e+a-n)%100;
   h -= (i-m)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F21(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   g += (h+e)%100;
   a += (g*e+l)%100;
   c += (a+m*j-a-f+d+i*a+l)%100;
   k -= (a*j*n+a-i-c+m-h+i)%100;
   g -= (g*f+b*c*b*g+a+f*i-e+a)%100;
   d -= (m-g)%100;
   l -= (i*a+k+b-d-k+h+e-k)%100;
   f += (f+j)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F22(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   k += (g-l-m-i+c*f+h-m+k*c)%100;
   k -= (h+j-k+n-c-l+b-n+f-k*m)%100;
   b += (i+c-c+i-l+a+g)%100;
   e += (f+l+a+b+g)%100;
   j -= (f-k*f-g+m)%100;
   a -= (c*f*m*h+i+a*l+f)%100;
   n -= (j-b-b-a-a+a*l*n)%100;
   h += (b-d+e)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F23(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l  = (k-k-e-i+j*h-f*b+i*j-n*n)%100;
   c -= (k+m+c+i*a)%100;
   a  = (m-j)%100;
   e -= (f*h+b-h-g)%100;
   m += (f+f-m-j-k-d*l)%100;
   k += (j*g-i-d*j)%100;
   a += (i+j)%100;
   d += (b+c)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F24(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   g += (n+h)%100;
   n -= (h-f)%100;
   i += (n+n-b+j-d*e+a)%100;
   b -= (h*n-j-j-c)%100;
   m += (g*k)%100;
   l -= (a-n-e+k-f*j-f+g+b*h-f-a)%100;
   n += (c+c-h+h+f+b*f)%100;
   i += (a+i)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F25(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   d += (e-k-b-m+f-d+c)%100;
   d += (j*l+l+e-m*n+b)%100;
   e += (e-l+a+c*k-f-l-i-i-d+h+d+m*b)%100;
   j += (b-m+n+e-c)%100;
   k += (a*h*g+c)%100;
   k -= (a*g+a-k+c+b+n*i+f-b-b)%100;
   d += (m*i)%100;
   d += (i-n-k-m*b+g-g+h-e-h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F26(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   e -= (n-m*c-j+m*l-g*k+a+g+a+g+k)%100;
   j += (b+e+m*f+e*e*k-n-g-b-c+e)%100;
   h += (d*e+l-i-b-b+h+e-h-b-m)%100;
   k += (d+e+k-n*m+a*f*a+f-g-d)%100;
   e += (k-e)%100;
   c  = (b*k-h+i+h-c+k*n-h+h-h*a*h)%100;
   m += (m-j*h*h*j-j+l*e-b+c+i)%100;
   g -= (k+j-m-j-b-h-h*e*f*e+e+n+g+b)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F27(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   a -= (j-h*g-f+a-b)%100;
   b -= (b+b+m*g-d+j+f)%100;
   d += (h-m-j+m-l*k+j*b*c-n*l-c*l+i)%100;
   d += (k+l+h*a+m*d*h*n-e*d+e-n-m-b)%100;
   b -= (k+g+l+l-m-f-a-k-g*e-e)%100;
   d -= (f+j)%100;
   k += (i-f*g*j-g-e)%100;
   d -= (n*l-d+j+i+f*i)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F28(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   e += (n-f*e+d+g*m)%100;
   f += (h+j+g+l)%100;
   e += (h+j*b+m)%100;
   h += (h-j-j-f+b)%100;
   f += (d-n-f-b+b)%100;
   f -= (n-i*d+l)%100;
   i += (l-n+a+c*b-j+j-c)%100;
   a += (j-e-f-h-h+a)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F29(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   e -= (h-c)%100;
   e  = (j-f-k+c-m-i-l+f*d-d+f-m+d)%100;
   g -= (g*b-c-m*k*n-c*b-d-h)%100;
   f += (f+d-d-i)%100;
   j += (b+i-j+m+l+b+j+b-f*f+l-c+m)%100;
   g  = (f+c+b+b+m+k-l*m-k+g+g*d)%100;
   m += (i+l)%100;
   i  = (e+c*d*l*j+h-i+b*m+a-b-d)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F30(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   j -= (g-h-d)%100;
   a += (a-m*l-l-h*m-n)%100;
   h -= (e+f+m+i)%100;
   l += (d+l+i*i-d+l+e+g+d-a+l-m-a+j)%100;
   g += (l*f-j-b+n)%100;
   k -= (d+l+f-j-d+c*h*n-l+e*g+c)%100;
   a += (j*a+l-n-d-e*k-c*n-g+m-b)%100;
   k -= (e-i*l-k*f)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F31(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l += (e-h*h-l-h-h-n-e-d-k-i)%100;
   h -= (j-h+g+f+f-e-l-i)%100;
   f += (f-h+k+c-m+h-k-j)%100;
   n += (d+k-i-l*g-n-l-h+g+e+g*k-d)%100;
   f += (m-j-d+m-j+g-h*m)%100;
   m -= (m+a*e*d+i-e)%100;
   l -= (k-b+h+b+k*j+h*h-a*e-a-a)%100;
   j -= (a*d+c+d+g+i+b)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F32(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   a += (d-i*m-l-h-k-g-m)%100;
   m -= (l+m*m+g+j-h*h*j)%100;
   f -= (l+i*i+e+n*k*n-f*m+m)%100;
   a += (l*l-i*d+f+j-d+f*b-k)%100;
   i += (h*a*g+k)%100;
   c -= (m+e+b+f-h-i*k-g)%100;
   k += (l+d+i-b-g-b+e-b-c)%100;
   e -= (e+b)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F33(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   i += (f-g-k-b)%100;
   l  = (i-e+i-m*k)%100;
   i -= (i+j+e-i+n+a+h*l+f-b+c)%100;
   b -= (b*h-c+a+h-a+n*j-l+k+l+d)%100;
   h  = (j-h-l+h+i+l+f+j*g-i)%100;
   c -= (e-k*a*m-l+a)%100;
   j += (e+d+i+d-c-l-n*g+e+g)%100;
   k -= (h*g+h-d+e+k-a*k+e+b+a)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F34(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   h += (a+h-b-j-f*l+h)%100;
   h += (j*b*g-l+a+i-h-e*c+h*a)%100;
   i += (i-a+e+g-a-a-n*g-b*n-j+k)%100;
   b += (k-l*k-j-f+f+b+f*k+f+b+c+m)%100;
   j -= (l*f)%100;
   j += (j+c+f)%100;
   l -= (n*a*m-k)%100;
   i += (n+i-g+l+h+e)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F35(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   g -= (l+k+a*k+h*l-j+c-f)%100;
   f -= (g*k-f+n+j)%100;
   h += (e-a-c-i+n)%100;
   e -= (n*h*k-c+a-m-d+f*k-n)%100;
   l -= (f+n+f-a)%100;
   k -= (m+e*k+a*a)%100;
   g += (c-h-c-h+e+g+n)%100;
   f -= (n+n+m-f*e-k*h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F36(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   m -= (a+a+l-d*k-f)%100;
   g  = (e-j+k-d*d*k+d*e+b+a+k)%100;
   n += (a*h-a+e*f+j-g-e+e-n-j+m-g-k)%100;
   i += (b-e-k-m-j*f+g-l)%100;
   c += (a+d+n-h+k*n+d-m+b)%100;
   l -= (f*i-i-e*d-m-l+k)%100;
   c -= (g-c+f*h-i*a-c+d+b+b)%100;
   d += (j-f+i-f+l-l-f+e)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F37(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   j  = (a-f-m-g-d*k*h-b-f+g-f-l+k-j)%100;
   a += (d+g-h*j+h-k-g-i*f+k+m)%100;
   e -= (h+n*d)%100;
   e += (b-d+b+a)%100;
   f -= (n+f)%100;
   k -= (g+l)%100;
   e += (h+a+l-e*d-k-e+l+i+i)%100;
   j -= (e+k)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F38(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   d += (c+d-e*i*l-j*a)%100;
   h += (b*d-c*d-c*f+g-b-e*k-g+h)%100;
   m -= (n-m*j*e-f-h)%100;
   c += (f*d+m+g-g+f-n-h)%100;
   h += (c-m+m-e-j-i*b-k-f-j-j-n)%100;
   n += (k+g+b+l+c+f+g+i+h-d-b-m+l+e)%100;
   g -= (a+b+h-m-b+h+g)%100;
   m -= (d-j*m*n-l-b+i)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F39(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   m -= (k+k+k-f-n+g)%100;
   j -= (i+g-g-m-j-j+b-e-a)%100;
   g += (f*c-c-b-m-a+k)%100;
   i += (n-b)%100;
   m -= (f-e+d-i*n-a+c-n-n+b+j+l-l*k)%100;
   l -= (g+b-m)%100;
   a -= (h-e+f*e+h-e*e*i+m*f-f+f+a-k)%100;
   f -= (c+k-g+k*g-m-d+j)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F40(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   k += (k+f+l*n+m)%100;
   d -= (e*d-d+b-g*l*a+a)%100;
   l += (c+m)%100;
   j -= (e+l*e-c-j+c-c-f+e-n-h)%100;
   j += (m+l)%100;
   f -= (b*d*h*h-n*h*m-m+j-f)%100;
   m += (m+c*i-f*d-n*f-g+h)%100;
   j += (j+j+a-a*e-e+b+c+n+c-n-e)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F41(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   m += (d+l*f-h-f-n+h-d+g-m-e+e)%100;
   e += (h*k+f+a*f+d+g-j-a*h+g*d)%100;
   f -= (f+d+n)%100;
   e += (n-k+m+e-b)%100;
   k -= (h-l+k-j-e*e+c-d+i*a-l)%100;
   l -= (n-n*j-i*e-c+g-j-m-c)%100;
   i += (l*h+n*k+g*n-m-l)%100;
   a  = (a+h*g*m+k-e+i-c-k-h*n+j-d)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F42(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f -= (f+c-i+h+a*b+f+j)%100;
   d += (m+m+j-l)%100;
   m += (i-n+i+i+e+a-h*j-n+c)%100;
   k  = (c*l+i+f-a+m+n*b+e-l+d-d)%100;
   i += (j+j*i-a+h+h-a-a*d-n)%100;
   k -= (g-g)%100;
   j += (m-h+d*m)%100;
   i += (i-d-d-j)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F43(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l -= (g+h+i-c-i*d+d-l*b)%100;
   e += (b-e-j-h+b)%100;
   m -= (m+c+g-j)%100;
   g -= (f-e-f-i+n*b-h+h)%100;
   g += (n-m+f-m)%100;
   n  = (b*k+d+m+i-d+m+e-f*l+n*i+h-n)%100;
   d -= (n-f*k*f-d-g+f-e+h-b*b-j*c+b)%100;
   a -= (d-a)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F44(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   b += (f-g*i-g+c+f)%100;
   b -= (d-f-k+c-e+f+l*c-a*n-g+a-g)%100;
   i -= (j-b)%100;
   n += (m*i-i+m+g+j*m+n-g)%100;
   d += (f*n-d-e+i+m)%100;
   g -= (c-a-e+i+k*i-j+m+k-g+b)%100;
   b += (i-l*i*f+l-g*e)%100;
   h += (c-i*d-n+k)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F45(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f += (k+i-d*a-j*d-f-g+m+j+k+g)%100;
   d -= (d-k)%100;
   k += (f-e)%100;
   h += (m-e)%100;
   e -= (g+k)%100;
   f += (g-l-j*d*e+c)%100;
   m += (e-h)%100;
   k += (g-l+f+c*l+f*i+i)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F46(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   g += (m*e+i-a+g-n*j+c+j-i)%100;
   c += (k-c*e*k-d-m)%100;
   d += (h*h+i)%100;
   i += (j-m-a-m*f*n+k-d-i)%100;
   c -= (e*n-b+n+l+i+d+l)%100;
   f -= (j+a+d-f*m)%100;
   l -= (l-n+j-c-g-l-b-m+l-m-d*a+a)%100;
   j += (n+b*a)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F47(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   a += (l+b*h-m-g-d+h)%100;
   d += (g-c-j-b+a*m+f-h+e-m-g+a-k)%100;
   k -= (n-i+e+n-a*g+l+m+h-g+f-f*m)%100;
   f += (f+l)%100;
   k += (b+c)%100;
   n -= (l-j)%100;
   m -= (d-a+l)%100;
   j += (l-n*m-j+n)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F48(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   m += (l+n*m+a+a+g)%100;
   d += (m*k+d+e-f+l-d+e*n+a)%100;
   a  = (d-k*d+n*j+a-m-e+g-a+i+f-i+e)%100;
   k += (g*d+b-i+b+e*l)%100;
   d -= (a+h+d-n*d-c*b-b*i*j-l)%100;
   b += (a+c-f-d*m*k+c)%100;
   f  = (c+j*a-f-i)%100;
   n -= (l-e+a+g-n-d)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F49(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l -= (a+h-f-l+n*g+d-m+c*g+j-i+j*e)%100;
   d -= (i+i*g-c*d+f-f*i-n-b)%100;
   n += (d+d)%100;
   e += (m*j-m-d)%100;
   a += (b-l*g+a*b+f+e-a)%100;
   b -= (g*d+e-h*h-l+l+a-h-c-c)%100;
   l += (k-c+d-c+h-m+h+c-b*k+n+c-g-i)%100;
   h += (a+d-f+k+d+h-n*f-e)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F50(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   c += (k*c-h-h-a+d+k+b)%100;
   g += (a*d)%100;
   f += (h+i-i+d*a)%100;
   e -= (i+f)%100;
   c -= (h+e+e*c-g*j-a-g-k*i)%100;
   b += (l-c*m*a*l*c+b+n+j*a-b*l)%100;
   g -= (l*n+l-d*n-e*i+i-g+f+l)%100;
   e -= (i-g+m-k+h*k+h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F51(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f += (g+h*e-i-h-g*i+b+n-j*m*k+k)%100;
   m += (m-f-m*c+m)%100;
   h -= (j+b+l+e+a+b*m*d*i-c)%100;
   a -= (h-g+a+c-h+g-n+f+b-j)%100;
   b += (i+b-i+j*g*k+m*d)%100;
   f += (f*b*k-b+b*n+b+k-h)%100;
   e -= (n-l+e+n+e*e+m*b*a+h+d+h)%100;
   j -= (n+k+m-g*j+f*b*a*f+k-i)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F52(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   k -= (a-g+k-i-m-a-b-n+j+a+b+b+i+e)%100;
   a -= (g-e-g*g-i*e*e+n-h-m+k)%100;
   n += (d+d-b+g-b-h)%100;
   c += (g+n+g+e-b*i)%100;
   a -= (a-g-i+m)%100;
   k += (j*l*k-e-c+d-l+d+f)%100;
   j += (f+d+g+l)%100;
   l  = (d-b*d-e-l*a+j*d*f)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F53(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f += (h-f-a+a-i)%100;
   i -= (e-c*c-b+g-m+g-f-c)%100;
   k += (c-e+i+n-d+d+i*f*g)%100;
   c += (a+m)%100;
   h -= (a*m+l+h-d)%100;
   h += (c+n-l+j+n-k*k-k+d*l)%100;
   l -= (e*a+k-c-l-d+g-h-f+d*h)%100;
   a -= (k*k+k-c-d-l*k-h-d-n)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F54(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f += (b-a*d*h+g-f-b)%100;
   k  = (m-k+h+n-b*h-h+i-i-j*c-i*h)%100;
   h -= (b-e)%100;
   j += (d+h+h+h*l+h-i-d-k-c-c-n+l)%100;
   m -= (n+h+k*j-m+j-f-b-f+b-c-m-h)%100;
   e  = (g-m+a+g-a-d-d-m*e+k-e)%100;
   h += (j-l+j+i)%100;
   b += (h*a-a+e+d+f-j*i-k-g)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F55(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   c += (b*n+c+a-j-c-n*k*j)%100;
   h -= (n*m-l*f+a*g-i+i-m-e)%100;
   l -= (e+i-f+f*c*e-i+a-h-k)%100;
   i -= (c+n+k+g)%100;
   j  = (a*a+c+n-d+b+g-m*e-m-l+f)%100;
   g -= (i+h+k-l+b+e)%100;
   g += (n-i)%100;
   m -= (h-k+m*c*j-l+n)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F56(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   a -= (h*a*e+g)%100;
   n += (c-a+k*g+l+b-i+e)%100;
   l += (a-g*n+m)%100;
   h  = (l-j-l+a+g-k+f-i-m+m)%100;
   c -= (h*d)%100;
   k -= (k+h-b+f+i*g+i+n-j+n)%100;
   c += (d-k+g*f+d*e-c+c+j*d)%100;
   j -= (l*f-j-n-c*d+n+b+h+a)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F57(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   a -= (j-h-f-h-l*l-h*a-m-c+j-l+n)%100;
   c += (l-b+b*a+i+b*h+a-d+j*d-b-k*j)%100;
   d += (m-f+b-j-l)%100;
   k += (a+k-a+i-n-k*n+b*e-g-k-a+m)%100;
   h += (e*f-j+g+n-d+d*m+j+a*m)%100;
   n += (l+m*e*n-n-g)%100;
   f -= (i*f*g-f-g+f*b)%100;
   c += (c+n-j+l+k)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F58(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   n -= (i-f+b+f*g-b+i*a-m+i+g-i)%100;
   n -= (h+i+m*d*h+k+k+f+m)%100;
   b -= (k+b-g-g*g-f-b*n)%100;
   l -= (n*h-e-e+j-c+d+n-d*h*b)%100;
   f -= (d-a)%100;
   k += (i+k-c*n*e+k-h+m-j+l+l-a+k)%100;
   a -= (f+m*k+l-m+h-j+d+l-j+h*g-j-h)%100;
   e += (f-c*l+g-i-j)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F59(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   n -= (c-a-c+a+k+c)%100;
   j -= (e+f-e*l-g-b-j-a+l-h-i+d+e+k)%100;
   k += (b-n-i+j)%100;
   m  = (e+h+e-d+i+m)%100;
   m += (n*g-f-d-c+l)%100;
   e += (i-m+c)%100;
   a -= (e+j-f*b+k+i*m)%100;
   n += (f-n*c+f*a)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F60(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   b -= (h+c+f*m+g+i+l-c+a-j*n-n+k)%100;
   b -= (i-m*m+k+d+l+e-g+f-d+a*f-a)%100;
   g += (b*l+j+d+d+n+l-m*h+k+a)%100;
   a -= (k+b+f+c)%100;
   g -= (g-h-m+l*h+c)%100;
   f  = (h*c+e*f-g*l-f-f)%100;
   d += (e+m-k+k-k+d-d)%100;
   l -= (j-l*g+a-a-e-f)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F61(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l -= (n-l-l+a-f-k+c-k*i*j+j)%100;
   g += (d+j+f-j*d+d-j-n+i-a)%100;
   c -= (d+g-g-f-f+i*d-j+i-b)%100;
   g += (k+j+n-i*b+d+b+d*g-j)%100;
   j += (h-g-m-e)%100;
   h  = (n+f*k-n+f*a*n+l+e*d)%100;
   a += (g-c*b-d-c-b*a+j)%100;
   b  = (e*a-c*f-b-g-b+b+b+g+n+c-b-a)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F62(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   n -= (e*a*c+a-d-n+b+l-g)%100;
   f += (i*j+b*l-c*g*i-h+l*e+i*i*h)%100;
   k -= (a-k)%100;
   e -= (e-k*h-b-c)%100;
   d += (e+j+h+j)%100;
   n -= (d+g*e+b*m)%100;
   h -= (k+f*k*a)%100;
   m -= (j+a-b)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F63(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f -= (h+j-h-d*j-c+k*h)%100;
   n += (l+m-c-j-n+b+f)%100;
   n -= (f*l*i)%100;
   g += (n+j-m+l-a*e+m-b+e+a-b)%100;
   n += (n+e-k+j-k+b-f-a)%100;
   c += (h+b*j*c+m-e+f)%100;
   f  = (b-a+b+m-d+e)%100;
   k += (e*e)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F64(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   g += (g+h+k)%100;
   j += (a-i+c*k-k*j-l-b+b*n-c*f+e)%100;
   c -= (l-g-j+a-b)%100;
   g -= (c-c)%100;
   g += (j*g-j-j+d-c+a+c+i-d*b*a+k-a)%100;
   h += (m-f*a-a-g*b*f-n-k-g+i+h-a)%100;
   c += (m-d+b*c-e*c)%100;
   m -= (i+b+m-m-l+n-l-g+j*k+n-e+h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F65(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   c -= (g+i*g*g+j)%100;
   j += (h+k-f+n-m)%100;
   d += (k-e*h*n+k)%100;
   k += (a+b+n+e*k)%100;
   n += (f+l-d-j+e+m)%100;
   m -= (j*l-l-k+b+b*j-n+m+b+b+c)%100;
   n -= (m-j+h*n+a+n*k+f*h*h+m)%100;
   f -= (n+b-c-f+k)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F66(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   a += (d-e+j*m-g-a-g)%100;
   f += (h*i*h-g+a-e-a*n+f*d-i-a+n)%100;
   i += (k-i+b*c-g-c+n-a+h*a-a)%100;
   f -= (j-k)%100;
   i -= (g+n+b-h+n+a)%100;
   k += (b+g-m-h-b-e-b)%100;
   k += (l*f-m+l-h*a-e)%100;
   a += (c+b+a+b+k+d)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F67(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   j  = (a-m-n+k+c-j+f)%100;
   a += (f+e*n+g-l)%100;
   a -= (c+m+i+b*m+b+n-e-b)%100;
   g -= (g+d+h-i+j+c*b-k+l+e)%100;
   a  = (d-f+f-d-j*c)%100;
   a -= (h+k*i+d*k+n-a+f-i-m-b)%100;
   m -= (h*f-b+h-c-d)%100;
   d += (i-c-n+k*i-b-k-g+a+b+g*h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F68(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f -= (b-d+g+i+m*d-a+n-a*a-a-f)%100;
   b  = (a+f-l-j)%100;
   c -= (h-l-e-n+l)%100;
   l += (e*f-b+b+l+d+e-k-h+f)%100;
   b -= (m+d-d*c+n*e+b-l-g-b+h+f-d*b)%100;
   k -= (h+e-m-l)%100;
   a += (g-h-d*l-i-n)%100;
   b  = (b+l+a-h+c)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F69(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f += (n-j-n*k-h+c*i-k)%100;
   l -= (g*c)%100;
   i -= (n+n-k-l-c-c-m+g-b+m-n+b+m+f)%100;
   g -= (j+k+h*c-g*a)%100;
   g -= (l*g)%100;
   d -= (j+g-f-f)%100;
   h  = (b-c+j-a+f+f-m+j)%100;
   j += (e+j+l-l+l+l+i)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F70(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   d += (a+f-f+f+i+f-k+b-a+n*d*f)%100;
   m += (k+f+k-a-i+i-k+i)%100;
   m -= (l+l+c-m+l+k)%100;
   i  = (a*b-j+c+b-j)%100;
   a += (k-c-b+a-h-h)%100;
   a  = (h+g*f+n-m*f-f-a*c-b+a+d+j)%100;
   i  = (h*b-b-j+j-k-g-g+l-h)%100;
   c -= (m-l+g-j)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F71(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   b -= (l+e-f-f+f+g+h+n-g+m)%100;
   c += (c-b+c+d+a)%100;
   i += (i-c-l*j)%100;
   l += (g+f-h*i+c+c+d+i*h-g-b+a-i-k)%100;
   m += (f*d+b+a+b+e+e*a+b)%100;
   h  = (c+e+i-e-j+c*g)%100;
   h += (k+n+c-e*c+k-d*e*n+h+m+e-a-b)%100;
   l += (c*i-d*h-i-g)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F72(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   j += (d-k-d)%100;
   m -= (a+f+g-a+m)%100;
   c += (k-d)%100;
   m  = (m-j-m+l+h)%100;
   i += (e-m+e-h+k+i)%100;
   c -= (m-d-d+f+n+d-d+m*m*b+j-h-a)%100;
   l += (b*g-k+h-c-a-k+b+h-h-m-n+e-g)%100;
   g -= (e+e*l*h*i+e-c+l*m+i*a-h+b-b)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F73(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   a += (e+j+h-g-i*h)%100;
   l -= (d*l+b*l-d-e*e-a-c+i+c+d-e*m)%100;
   k += (k+l)%100;
   c  = (l*c-f+c+e-i*i+j+l-g+c)%100;
   f += (e+j+l*b+i*c+n-m-n-l)%100;
   k  = (l+m-h)%100;
   d += (m-d+c+m-i-g-m-c-l-a*c-j-a)%100;
   m += (d*d)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F74(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   l -= (c-e-c)%100;
   a -= (c-i-g+g*e*k*j-c*g)%100;
   d += (f-l)%100;
   a += (e+c-g-e-l-e+h+c+n+b+f-e)%100;
   e += (f-m)%100;
   a += (a-n+i*d-a)%100;
   h -= (d-n*c-i*l-f-m)%100;
   j += (j+f-i+c-e+b*h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F75(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   a -= (e-g+e+g)%100;
   m -= (a*c)%100;
   g -= (h-k-i*i-f-c+e-n-k+a+c+b)%100;
   f  = (j+c+c+n-i)%100;
   c += (n*m)%100;
   b -= (f-j)%100;
   j -= (b-l*g-f*h+k+c-a-g)%100;
   f  = (m+g)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F76(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   d  = (e+m*k*f+d+k*j)%100;
   b -= (n-h*k*g-l+c*l+m-g+f+g+h*i-a)%100;
   b  = (e-d)%100;
   d += (b-l-n*l-j-f+m-m-b)%100;
   n -= (f+l-i*c+d-i)%100;
   g -= (n-h-n*j+e-h+h-f+a+c*g)%100;
   b -= (a-a-d+c+j+l+d+e-b-m+c*h)%100;
   d -= (l*c+i+f-m)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F77(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   b -= (n*g+c+b+h*a-m*b+n*m)%100;
   b += (c-h*b*k*f+k*m*e-k)%100;
   c -= (g-j-k-b+f-n-h*a-f-n+n-b)%100;
   i += (a+l*e+d*k+b+m)%100;
   n -= (e-k-c*e-m-i-i*k-j+c-f+d-i-j)%100;
   g -= (k-h)%100;
   h -= (f-j-n-g-k*n-c-d-h-d)%100;
   m  = (k-g+i+f-b-l+i+j-c+c+m*k-a)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F78(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   h += (d+d-j*f-b-e*c-e+c*l)%100;
   i -= (m-h)%100;
   n  = (d*l+i+m+c-l-i)%100;
   h += (f-a+e-n-i+j)%100;
   f -= (l+f+h+h*l*n)%100;
   c -= (f-l-k+m+i-e+g-a)%100;
   i += (k+g+i-l+h+e+h+l-d-i-i+d+j)%100;
   c += (b-m-n-f-i-b-j*l-d-m+a-j)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F79(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   g -= (i+m+i*f+j-i+h+l+g)%100;
   b -= (i-n+e+b+j+d*l*c-m-e-f-d-j)%100;
   l -= (c+l*n-j+n+h+d*n+c+c+c*i-g*l)%100;
   d -= (a+c-a+d*m*b*l*l-m+b+f)%100;
   f -= (i+j)%100;
   j -= (g+n*f+n)%100;
   g -= (g+k-i*i-e-j-d-m)%100;
   m -= (j+b+c*f+n-h+n+n-k+h+l+f)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F80(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   c  = (k-a-f+l-g-e+e-a-h+n)%100;
   l -= (g+k+d-k-c-i-e+n*i*g)%100;
   l += (k+e*j+d-m*j*m)%100;
   a += (m-k-k)%100;
   f += (a*b+h+g-j*b+d*k+j+j-k*j)%100;
   l -= (g+d*h-e*g+a*a+g*e)%100;
   a += (m-h-n*k+d+e*e+e*g*a)%100;
   k  = (i+m+f-h+b-g)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F81(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   k -= (g*e)%100;
   a += (e-d*h*h+a+j*n+n-g-e-g-f-l+g)%100;
   b -= (e+h*c-i-d-j-k*c*m*k)%100;
   h  = (m-c+f+d-m)%100;
   f += (c-g-e+l+g*m*d*d*g)%100;
   c += (i+e-b)%100;
   e  = (j-j-c-h-j+b-c-b+c+b*a)%100;
   e += (g+n-j)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F82(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   b += (h+a-f+d-a*e)%100;
   k += (h+l+e-h+c-a-a-j)%100;
   n -= (f-b+l-b-e)%100;
   b -= (g-j+c+a-a+f)%100;
   a -= (j+k+j*j-g+b)%100;
   m -= (h+e-a-h+k)%100;
   k += (f+g-d+n-j*i*i-m-h*l)%100;
   j += (h+h-f+i-a+h+a*f-h+n-j-k+n-k)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F83(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   j += (e*g+n*k+n-k-m-g-a+m-h*n)%100;
   i += (e+m*j+c+a-f-m-i-i*j)%100;
   h += (a*a-n*m)%100;
   h += (c+e+b+a+l*h-g-a)%100;
   c -= (f+n*m*h+i-k+i-c-f-j*i+g+i)%100;
   j += (a-k+h-d*k-l-l-j)%100;
   a -= (b+a)%100;
   j -= (a-m+e+a*f*d*j*k-n+h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F84(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f  = (j+a*f*d)%100;
   f -= (f+c-h+l-k+n+i)%100;
   i -= (m+g-i*j-l)%100;
   d += (f-m-c+m)%100;
   a += (b*j)%100;
   a -= (g+b+k+j+g)%100;
   e  = (h+g)%100;
   f  = (m*e-i+d)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F85(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   b -= (a-n+e*e)%100;
   l -= (a*k+b+e-k-d)%100;
   l -= (a-j+d+a+l-e-f+j+l*c-b+i)%100;
   g += (h+j+a)%100;
   d -= (i+m-m)%100;
   f += (e+h-n-a)%100;
   f  = (f-a-g+a+a+k)%100;
   i += (i+m+c+n-f-j*d-e*k-i-c)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F86(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   n -= (m-h*a-l)%100;
   e -= (b+e-h+b*j+j-b)%100;
   f -= (c*k*f-m*f-h*f+a-h*f-k*g)%100;
   b += (b-g-k-l+b+b+g-d+h-k-f+l*i)%100;
   d += (j*j)%100;
   b -= (c*c-b-e+m+e+b+m*l-h)%100;
   l += (i+m)%100;
   g += (e+h+m-h+g)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F87(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   n += (k-i-h-j)%100;
   e += (a-k+b-n-i+a+l)%100;
   i += (n-g-j-j+d-a-c+b+g)%100;
   a -= (d*f+c+m-d*j-g+c-i-m*l*c)%100;
   c -= (j-b-g+h*i-b-n*e-c-e*h*g-l-e)%100;
   n += (k-i-l+d-g-b+m*h+i+k)%100;
   c -= (m-c)%100;
   a -= (a*e-h-d*l*e-l)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F88(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f -= (k+l-g*b+m+h-n*l*h)%100;
   m -= (c+c*d-g-m-k*n-g+l)%100;
   j -= (j-d+n-a+m*n)%100;
   e += (l-b-h+a+d-g)%100;
   g += (n+c*j*i)%100;
   a += (a*m-g*d*h+g-e+m)%100;
   n += (d-e+d-i-n-l-c+h)%100;
   d -= (m+b*f+l)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F89(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   j += (e+h-e)%100;
   k -= (l+b+i-m-l-f+a*h-l)%100;
   l -= (m+h+b+g-j-c*a*m+l*m-n-j-c+f)%100;
   m += (g+j+d-e-g)%100;
   m += (g-a)%100;
   l += (m+k*c+g-h-c+i-n-k+l+k)%100;
   h += (c*d+c+l+g*b)%100;
   n  = (e+k-b+l-c+f*h-b)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F90(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   n += (m+j)%100;
   h += (i+e+e)%100;
   d += (d*e+m-b+g+m-j)%100;
   k -= (g*d-i-i)%100;
   b -= (f+g+a+h*f*n+a+j-m-f+h)%100;
   f += (n*m-g+l+e-m*k*f-m+b-a+j)%100;
   e += (g+b+i+i*m+d-j*d)%100;
   b -= (b-c)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F91(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   j += (f*c*e-i+c+m+d)%100;
   l  = (j-n-e-n-i*l*e-b-e*i+d-e-n+b)%100;
   j += (b-m)%100;
   b += (m*b-n*a)%100;
   n += (l+n+b)%100;
   l  = (i-g)%100;
   e += (e+a-m*h-l*k+f)%100;
   e  = (f+e)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F92(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   a -= (h+k-d-l*d-h+d+h-e-i-j-n)%100;
   i -= (g-e*n+f*h-l+c+f+a-k-k)%100;
   a -= (e-n+i+f-d-b+i-d*k-j+e+n)%100;
   g -= (g*c+k+f)%100;
   i += (m+h*i+g-j+f+i+g+h-j+k+i)%100;
   m += (e+m-l+c-k+g+c+d*n*e-g)%100;
   e -= (l*e+j-m+e-k-g+i-c*a*n+e)%100;
   c += (i+d+k-e+e+i+l*l+a*g-h)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F93(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   k += (c*f-l-g+j)%100;
   i  = (e+l)%100;
   c += (g*d+b+i-i-d-f-i)%100;
   d -= (f-i)%100;
   b += (k+k-i*i)%100;
   g -= (h+m+j-b)%100;
   k -= (l*d)%100;
   a -= (j*e+g+b+a+h-e+k)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F94(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   f  = (l+c+n-i+n-m-d+b-h+m-f-d-n-f)%100;
   h -= (a+g+a*d*l)%100;
   j += (i-c-m)%100;
   l += (k+f+g+i-j-m-g*l-e+g)%100;
   d  = (e-i+f-j-j-f+k+k)%100;
   j -= (k-l*n-e-g+c+l-j-i)%100;
   a += (j-a*c+b)%100;
   j  = (k-j-b-k*l-i+d-n*f)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F95(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   i -= (b-d-k*e+c+e-n+l+f-l+e+i)%100;
   m -= (j+m+l+f-i*l+i+e*k*g+e)%100;
   l += (f*a+f-b)%100;
   e += (b+f*f*d-g+e*h-c*a*i-j+a-g-m)%100;
   f -= (e-k-m-n+l)%100;
   k -= (d+k*i-b)%100;
   n -= (h-m+b-k-i-c+m+i*h+b-j-g+l)%100;
   n -= (a-a*e)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F96(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   k -= (g*a)%100;
   n -= (f*g+e-b+k*f+g+l+e-f+k*e+k-d)%100;
   j -= (j+l+n-f*c-n+i+b*c*j)%100;
   j += (f-j-l-b-a-c-g)%100;
   n += (a+d+g+f*b+f*a+l-j-c+e+g-i-i)%100;
   b -= (c-a+n+c-j+e-a+d+k-e)%100;
   j -= (g+i+n-m+g-n*j-k)%100;
   i += (c*f-e-n)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F97(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   i += (m-l-k+f-m+m+c+c-h+i-n-e+a-b)%100;
   m += (b-e+a+k*n-d+j+l)%100;
   b += (c-j+i*j-h+n+a-k-h)%100;
   d  = (n-j*m-d-d*h+h*n-a+b-e)%100;
   a += (e+b*e+e-i+a)%100;
   e -= (n+n-c-a*g-f+h-e-c+n*h*f*b)%100;
   f -= (i+c+m-k*n-k)%100;
   a += (k-h*k+l-g+l-l+j-k+f+m-b+k)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F98(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   i -= (k-f*m*d)%100;
   i -= (n+e+m+l)%100;
   b += (m-i+b*n+j-f-m-m+g*h)%100;
   j += (h+j+k-n-i)%100;
   m += (b+j)%100;
   f += (h-a-n+j-b-a-j*d-l-i-n-a-j)%100;
   n -= (h-g*l+e+h-d*j+m-l)%100;
   k -= (h+m)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int F99(void)
{
   int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
   a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
   e -= (f*a-i-j+a+c-d-m+a)%100;
   m -= (g+f)%100;
   j += (e-n+c*n-j-j-j+h)%100;
   f -= (e+f-i-n+n*m-g+k-i)%100;
   h += (m*b-d+a-c+g-m-k-g+g+l)%100;
   a += (b*h-n*d+k*d-e+f-d-j-h)%100;
   c -= (j-e-b-d+m+i-b+f+c)%100;
   m += (b-f+j-k-f-m+l)%100;
   return (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
}

int main(void)
{
   int I;
   clock_t StartTick = clock();
   for(I=0; I<285; I++) IFcnt[I]   =0;
   for(I=0; I<285; I++) IFEcnt[I]  =0;
   for(I=0; I<285; I++) SWcnt[I]   =0;
   for(I=0; I<285; I++) WHILEcnt[I]=0;
   for(I=0; I<285; I++) DOcnt[I]   =0;
   for(I=0; I<285; I++) FORcnt[I]  =0;
   long int sum=0;

   sum += F1( ) ;
   sum += F2( ) ;
   sum += F3( ) ;
   sum += F4( ) ;
   sum += F5( ) ;
   sum += F6( ) ;
   sum += F7( ) ;
   sum += F8( ) ;
   sum += F9( ) ;
   sum += F10( ) ;
   sum += F11( ) ;
   sum += F12( ) ;
   sum += F13( ) ;
   sum += F14( ) ;
   sum += F15( ) ;
   sum += F16( ) ;
   sum += F17( ) ;
   sum += F18( ) ;
   sum += F19( ) ;
   sum += F20( ) ;
   sum += F21( ) ;
   sum += F22( ) ;
   sum += F23( ) ;
   sum += F24( ) ;
   sum += F25( ) ;
   sum += F26( ) ;
   sum += F27( ) ;
   sum += F28( ) ;
   sum += F29( ) ;
   sum += F30( ) ;
   sum += F31( ) ;
   sum += F32( ) ;
   sum += F33( ) ;
   sum += F34( ) ;
   sum += F35( ) ;
   sum += F36( ) ;
   sum += F37( ) ;
   sum += F38( ) ;
   sum += F39( ) ;
   sum += F40( ) ;
   sum += F41( ) ;
   sum += F42( ) ;
   sum += F43( ) ;
   sum += F44( ) ;
   sum += F45( ) ;
   sum += F46( ) ;
   sum += F47( ) ;
   sum += F48( ) ;
   sum += F49( ) ;
   sum += F50( ) ;
   sum += F51( ) ;
   sum += F52( ) ;
   sum += F53( ) ;
   sum += F54( ) ;
   sum += F55( ) ;
   sum += F56( ) ;
   sum += F57( ) ;
   sum += F58( ) ;
   sum += F59( ) ;
   sum += F60( ) ;
   sum += F61( ) ;
   sum += F62( ) ;
   sum += F63( ) ;
   sum += F64( ) ;
   sum += F65( ) ;
   sum += F66( ) ;
   sum += F67( ) ;
   sum += F68( ) ;
   sum += F69( ) ;
   sum += F70( ) ;
   sum += F71( ) ;
   sum += F72( ) ;
   sum += F73( ) ;
   sum += F74( ) ;
   sum += F75( ) ;
   sum += F76( ) ;
   sum += F77( ) ;
   sum += F78( ) ;
   sum += F79( ) ;
   sum += F80( ) ;
   sum += F81( ) ;
   sum += F82( ) ;
   sum += F83( ) ;
   sum += F84( ) ;
   sum += F85( ) ;
   sum += F86( ) ;
   sum += F87( ) ;
   sum += F88( ) ;
   sum += F89( ) ;
   sum += F90( ) ;
   sum += F91( ) ;
   sum += F92( ) ;
   sum += F93( ) ;
   sum += F94( ) ;
   sum += F95( ) ;
   sum += F96( ) ;
   sum += F97( ) ;
   sum += F98( ) ;
   sum += F99( ) ;

   {
      int a,b,c,d,e,f,g,h,i,j,k,l,m,n;
      a=b=c=d=e=f=g=h=i=j=k=l=m=n=1;
      d += (b-n-k-n-g*g)%100;
      b -= (l-k+a*j+g+d)%100;
      a += (g*n-n-h+j*m*e+f+g+b-k)%100;
      f += (i-d*b+l*f+m-c+j+c*m*e*e+d-b)%100;
      m -= (m*k-i-f-l-i)%100;
      e  = (h+g+n-n-b+f*d-e-c-c+d-n-d-i)%100;
      b += (h-m+i+j+j-e+l-h-g-n*a-h+f+e)%100;
      a += (c+a*b*i-k-c+a*g-m)%100;
      sum += (a+b+c+d+e+f+g+h+i+j+k+l+m+n)%100 ;
   }

   cout << "\nChecksum = " << sum;
   for(I=sum=0; I<0; I++) sum += IFcnt[I];
   cout << "\nIF frequency:      Static = " << 0 << "   Dynamic = " << sum ;
   for(I=sum=0; I<0; I++) sum += IFEcnt[I];
   cout << "\nIF-ELSE frequency: Static = " << 0 << "   Dynamic = " << sum ;
   for(I=sum=0; I<0; I++) sum += SWcnt[I];
   cout << "\nSWITCH frequency:  Static = " << 0 << "   Dynamic = " << sum ;
   for(I=sum=0; I<0; I++) sum += WHILEcnt[I];
   cout << "\nWHILE frequency:   Static = " << 0 << "   Dynamic = " << sum ;
   for(I=sum=0; I<1; I++) sum += DOcnt[I];
   cout << "\nDO frequency:      Static = " << 1 << "   Dynamic = " << sum ;
   for(I=sum=0; I<0; I++) sum += FORcnt[I];
   cout << "\nFOR frequency:     Static = " << 0 << "   Dynamic = " << sum ;
   cout << "\nRun Time = " << double(clock()-StartTick)/CLOCKS_PER_SEC << " sec\n\n";

   return 0;
}
